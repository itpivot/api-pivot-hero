const moment = require('moment');

const db = require('../models');
const ApiMarketingCheckin = db.api_marketing_checkin;
const ApiMarketingCompanyMessengerActive = db.api_marketing_company_messenger_active;
const ApiMarketingCompanyMessengerUser = db.api_marketing_company_messenger_user;
const ApiMarketingCompany = db.api_marketing_company;
const ApiMarketingUser = db.api_marketing_user;
const sequelize = db.sequelize;

const getAttendance = async (req, res, next) => {
    const { user } = req;
    const { marketing_user_id } = user;
    const current_date = moment(Date.now()).format('YYYY-MM-DD');

    const checkinDetail = await ApiMarketingCheckin.findOne({
        where: {
            apiUserId: marketing_user_id,
            system_timestamp: sequelize.where(sequelize.fn('date', sequelize.col('ApiMarketingCheckin.system_timestamp')), '=', `${current_date}`),
        },
        order: [
            ['checkin_id', 'DESC']
        ]
    });

    if(checkinDetail) {
        const { checkinTypeId } = checkinDetail;
        if(checkinTypeId == 1) { // checked in
           
             res.status(200).json({
                 status_code: 200,
                 message: 'success',
                 checkin_status: 'checkin'
             });
        } else { // checked out 
            res.status(200).json({
                
                status_code: 200,
                message: 'success',
                checkin_status: 'checkout'
            });
        }
    } else { // not check in
        res.status(200).json({
            status_code: 200,
            message: 'success',
            checkin_status: 'new'
        });
    }
}

const checkIn = async (req, res, next) => {
    const { latitude, longtitude } = req.body;
    const { user } = req;
    const { marketing_user_id } = user;
    const checkinTypeId = 1;
    const current_timestamp = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');
    const current_date = moment(Date.now()).format('YYYY-MM-DD');

    

    try {   
        const profile = await ApiMarketingUser.findOne({
            where: { marketing_user_id: marketing_user_id },
            include: [{
                 attributes: ['companyId'],
                 model: ApiMarketingCompanyMessengerActive,
                 as: 'user_active'
            },{
                 attributes: ['companyId'],
                 model: ApiMarketingCompanyMessengerUser,
                 as: 'user_company'
            }]
        });

        let { apiRoleId: role_id } = profile;

        if(role_id) {
            if(role_id == 3) {
                // Rescue 
                let { companyId } = profile.messengerActive;

                const dataActive = await ApiMarketingCompanyMessengerActive.findOne({
                    where: {
                        marketing_user_id: marketing_user_id,
                        companyId: companyId,
                        start_at: sequelize.where(sequelize.fn('date', sequelize.col('ApiMarketingCompanyMessengerActive.started_at')), '=', `${current_date}`),
                    },
                    order: [ ['id', 'DESC' ]]
                });

                if(!dataActive){
                    const error = new Error("ไม่พบข้อมูลการ Check In");
                    error.statusCode = 500;
                    throw error;
                }

                await ApiMarketingCompanyMessengerActive.update(
                    { active: 1, system_timestamp: current_timestamp },
                    { where: { id: dataActive.id } }
                );
            } else {
                const messengerActive = await ApiMarketingCompanyMessengerActive.findOne({
                    where: {
                        apiUserId: marketing_user_id,
                        start_at: sequelize.where(sequelize.fn('date', sequelize.col('ApiMarketingCompanyMessengerActive.started_at')), '=', `${current_date}`),
                    },
                });

                if(!messengerActive) {
                    const setActive = await ApiMarketingCompanyMessengerActive.create({
                        apiUserId: marketing_user_id,
                        system_timestamp: current_timestamp,
                        started_at: current_timestamp,
                        companyId: profile.user_company.companyId,
                        active: 1
                    });
                }
            }
        }
        
        // check in
        const newCheckin = await ApiMarketingCheckin.create({
            apiUserId: marketing_user_id,
            system_timestamp: current_timestamp,
            checkinTypeId: checkinTypeId,
            latitude: latitude,
            longtitude: longtitude 
        });

        if(!newCheckin.checkin_id) {
            const error = new Error("การ Check In ผิดพลาด");
            error.statusCode = 500;
            throw error;
        }
        
        res.status(200).json({
            status_code: 200,
            message: 'คุณได้ทำการ Check In สำเร็จแล้ว'
        });
    } catch(error) {
        next(error);
       
    }
}

const checkOut = async (req, res, next) => {
    const { latitude, longtitude } = req.body;
    const { user } = req;
    const { marketing_user_id } = user;
    const checkinTypeId = 2;
    const current_timestamp = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');

        try {   
            const newCheckin = await ApiMarketingCheckin.create({
                apiUserId: marketing_user_id,
                system_timestamp: current_timestamp,
                checkinTypeId: checkinTypeId,
                latitude: latitude,
                longtitude: longtitude 
            });
    
            if(!newCheckin.checkin_id) {
               const error = new Error("การ Check Out ผิดพลาด");
               error.statusCode = 500;
               throw error;
            }

            res.status(200).json({
                status_code: 200,
                message: 'คุณได้ทำการ Check Out สำเร็จแล้ว'
            });
        } catch(error) {
            next(error);
        }
}

module.exports = {
    getAttendance,
    checkIn,
    checkOut
}