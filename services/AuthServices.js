
const jwtConfig = require('../config/jwtConfig');
const jwt = require('jsonwebtoken');
require('../config/passport');
const passport = require('passport');
const db = require('../models');
const { mode } = require('crypto-js');
const { Op } = require("sequelize");
// require('dotenv').config();

const sequelize = db.sequelize;
const User = db.user;
const ApiMarketingUser = db.api_marketing_user;
const ApiMarketingRole = db.api_marketing_role;

const signIn = (req, res, next) => {
    passport.authenticate('login', async (error, user, info) => {
        if(error) {
            res.status(500).json({
                    status_code: 500,
                    message: error.message
            });
        }

       if(info !== undefined) {
            res.status(401).json({
                    status_code: 401,
                    message: info.message
            });
       } else {
                const user_api = await ApiMarketingUser.findOne({
                    raw: true,
                    nest: true,
                    where: { 
                        apiRoleId: {
                            [Op.or]: [1, 3]
                        } 
                     }, // 2M
                    include: [{
                        model: User,
                        where: { username: req.body.username },
                        as: 'user'
                    },{
                        attributes: ['role_name'],
                        model: ApiMarketingRole,
                        as: 'api_user_role'
                    }]
                });

               if(user && user_api) {
                    // const expire = Math.floor(Date.now() / 1000) + (60 * 60); // current millisecond +1m
                    const payload = {
                        username: user.username,
                        role: user_api.api_user_role.role_name
                    }

                    const token = jwt.sign(payload, jwtConfig.secret, { expiresIn: jwtConfig.access_token_expires });
                    // const refreshToken = jwt.sign(payload, jwtConfig.refresh_secret, { expiresIn: jwtConfig.refresh_token_expires });
                    const expiry = jwt.decode(token).exp;

                    try {

                        if(!token || !expiry) {
                            res.status(403).json({
                                    status_code: 403,
                                    message: 'No Provide Token.'
                            });
                        }

                        const rowUpdate = await ApiMarketingUser.update(
                            { 'token': token, 'expires': expiry },
                            { where: { marketing_user_id : user_api.marketing_user_id } }
                        );

                        if(!rowUpdate) {
                            res.status(500).json({
                                    status_code: 500,
                                    message: 'Updating failed.'
                            });
                        }

                        res.status(200).send({
                            status_code: 200,
                            token_type: "Bearer",
                            access_token: token,
                            expires_in: expiry,
                            message: 'เข้าสู่ระบบสำเร็จแล้ว'
                        });
                    } catch(error) {
                        next(error);
                    }
               }
        }
       
    })(req,res,next);
}


const createTokenCookies = (res, token) => {
     const cookieOptions = {
        httpOnly: true,
        secure: false, 
        expires: new Date(Date.now() + 7*24*60*60*1000)
    };
    res.cookie('refreshToken', token, cookieOptions);
}



const signOut =  async (req, res, next) => {
    try {
        const { user } = req;

        const userData = await ApiMarketingUser.update(
            { 'token': null, 'expires': null },
            { where: { marketing_user_id : user.marketing_user_id } }
        );

        if(!userData) {
            res.status(500).json({
                error: {
                    status: 500,
                    message: 'Updating failed.'
                }
            });
        }

        req.user = null;
        req.logout();
        res.status(200).send({
            status: false,
            status_code: 200,
            message: 'User Logout.'
        });
        next();
    } catch(error) {
        next(error);
    }
}

module.exports = {
    signIn,
    signOut
}