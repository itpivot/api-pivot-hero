const moment = require('moment');
const path = require('path');
const fs = require('fs');
const util = require('util');
const stat = util.promisify(fs.stat);
const rename = util.promisify(fs.rename);
const access = util.promisify(fs.access);
const mkdir = util.promisify(fs.mkdir);
const cp = util.promisify(fs.copyFile);

const db = require('../models');
const ApiDipchipCardReader  = db.api_dipchip_card_reader ;
const ApiDipchipMessenger   = db.api_dipchip_messenger ;
const PvsWorkOrder          = db.pvs_work_order;
const PvsCustomer           = db.pvs_customer;
const sequelize             = db.sequelize;
const ApiDipchipCardReaderLog  = db.api_dipchip_card_reader_log ;



require('dotenv').config();

const getIDCardAll = async (req, res, next) => {

    try {   
        const jobs = await ApiDipchipCardReader.findAll({
            raw: true,
            nest: true,
            attributes: [
                'work_order_barcode',
                'work_order_id',
                'customer_id',
                [
                    sequelize.fn('AES_DECRYPT', sequelize.col('card_id'), `${process.env.DB_SECRET}`),
                    'card_id'
                ],
                [
                    sequelize.fn('AES_DECRYPT', sequelize.col('name_title'), `${process.env.DB_SECRET}`),
                    "name_title"
                ],
                [
                    sequelize.fn('AES_DECRYPT', sequelize.col('name'), `${process.env.DB_SECRET}`),
                    "name"
                ],
                [
                    sequelize.fn('AES_DECRYPT', sequelize.col('name_mid'), `${process.env.DB_SECRET}`),
                    "name_mid"
                ],
                [
                    sequelize.fn('AES_DECRYPT', sequelize.col('surname'), `${process.env.DB_SECRET}`),
                    "surname"
                ],
                [
                    sequelize.fn('AES_DECRYPT', sequelize.col('name_title_en'), `${process.env.DB_SECRET}`),
                    "name_title_en"
                ],
                [
                    sequelize.fn('AES_DECRYPT', sequelize.col('name_en'), `${process.env.DB_SECRET}`),
                    "name_en"
                ],
                [
                    sequelize.fn('AES_DECRYPT', sequelize.col('name_mid_en'), `${process.env.DB_SECRET}`),
                    "name_mid_en"
                ],
                [
                    sequelize.fn('AES_DECRYPT', sequelize.col('surname_en'), `${process.env.DB_SECRET}`),
                    "surname_en"
                ],
                'address_no',
                'address_moo',
                'address_sub_alley',
                'address_alley',
                'road',
                'sub_district',
                'district',
                'province',
                'sex',
                'date_of_birth',
                'location_of_birth',
                'date_of_issue',
                'date_of_expiry',
                'id_of_picture',
                'remark',
                'picture',
            ],
            order: [
                ['api_dipchip_card_reader_id', 'DESC']
            ]   
        });

        // console.log(jobs) ;

        if(jobs.length > 0) {

            var result = [];

            for( i = 0 ; i<jobs.length; i++ ) {
                result[i] = jobs[i];
                let {  card_id, name_title, name, name_mid , surname, name_title_en, name_en, name_mid_en, surname_en } = jobs[i];
                // let { work_order_barcode, card_id, name} = jobs[i];

                  result[i].card_id = card_id.toString('utf8') ;
                  result[i].name_title = name_title.toString('utf8') ;
                  result[i].name = name.toString('utf8') ;
                  result[i].name_mid = name_mid.toString('utf8') ;
                  result[i].surname = surname.toString('utf8') ;
                  result[i].name_title_en = name_title_en.toString('utf8') ;
                  result[i].name_en = name_en.toString('utf8') ;
                  result[i].name_mid_en = name_mid_en.toString('utf8') ;
                  result[i].surname_en = surname_en.toString('utf8') ;
                //   console.log(card_id.toString('utf8'));
            }

            // let result =  JSON.parse(JSON.stringify(jobs));
            // let { work_order_barcode } = jobs;
            console.log(result) ;
            // console.log(work_order_barcode) ;

            res.status(200).json({
                status_code: 200,
                message: 'success',
                data: {
                    result
                }
            });
        }
        
    } catch(error) {
        next(error);
       
    }

}

const saveIDCard = async (req, res, next) => {

    const { card_id, name_title, name, name_mid , surname, name_title_en, name_en, name_mid_en, 
            surname_en, address_no, address_moo, address_sub_alley, address_alley,road, sub_district,
            district, province, sex, date_of_birth, location_of_birth, date_of_issue, date_of_expiry, 
            id_of_picture, remark, work_order_barcode, work_order_id, customer_id, picture } = req.body;
    // const { user } = req;
    // const { marketing_user_id } = user;
    const current_timestamp = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');

    try {   
        // save information for Card ID
        const newDipchipCardReader = await ApiDipchipCardReader.create({
            // apiUserId: marketing_user_id,
            system_timestamp: current_timestamp,
            work_order_barcode: work_order_barcode,
            work_order_id: work_order_id,
            customer_id: customer_id,
            card_id: sequelize.fn('AES_ENCRYPT', card_id, `${process.env.DB_SECRET}`),
            name_title: sequelize.fn('AES_ENCRYPT', name_title, `${process.env.DB_SECRET}`),
            name: sequelize.fn('AES_ENCRYPT', name, `${process.env.DB_SECRET}`),
            name_mid:  sequelize.fn('AES_ENCRYPT', name_mid, `${process.env.DB_SECRET}`),
            surname: sequelize.fn('AES_ENCRYPT', surname, `${process.env.DB_SECRET}`), 
            name_title_en: sequelize.fn('AES_ENCRYPT', name_title_en, `${process.env.DB_SECRET}`), 
            name_en: sequelize.fn('AES_ENCRYPT', name_en, `${process.env.DB_SECRET}`),
            name_mid_en: sequelize.fn('AES_ENCRYPT', name_mid_en, `${process.env.DB_SECRET}`),
            surname_en: sequelize.fn('AES_ENCRYPT', surname_en, `${process.env.DB_SECRET}`), 
            address_no: address_no,
            address_moo: address_moo,
            address_sub_alley: address_sub_alley,
            address_alley: address_alley,
            road: road,
            sub_district: sub_district, 
            district: district,
            province: province, 
            sex: sex,
            date_of_birth: date_of_birth, 
            location_of_birth: location_of_birth,
            date_of_issue: date_of_issue,
            date_of_expiry: date_of_expiry,
            id_of_picture: id_of_picture,
            remark: remark,
            picture: picture
        });

        

        if(!(newDipchipCardReader == null) ) {

            console.log(' True ') ;

            let { work_order_barcode, card_id, name } = newDipchipCardReader;

            res.status(200).json({
                status_code: 200,
                message: 'คุณได้ทำการบันทึกข้อมูลบัตร ปชช. สำเร็จแล้ว',
            });
        }else{

            console.log(' False ') ;

            const error = new Error("การบันทึกข้อมูลผิดพลาด");
            error.statusCode = 500;
            throw error;

        }
        

    } catch(error) {
        next(error);
       
    }
}

const savePhoto = async (req, res, next) => {
    let error = null;
    let result_save = null;
    let new_img_text = '';
    
    try {
        const file = req.file;
        const {work_order_barcode} = req.body;
        const { filename: ori_name, path: ori_path } = file;

        // ต้องการหาเลขที่ ID Card
        let raw_card_id = path.basename(ori_path, '.jpg');
        let card_id = raw_card_id.substring(raw_card_id.length - 13, raw_card_id.length);

        // console.log('card_id : ' + card_id);

        if(file) {

            const ext = path.extname(file.originalname);
        
            let new_path = `./public/dipchip/workorder/${work_order_barcode}/customer/`;            
            let new_name = `${card_id}${ext}`;
            new_img_text = `${new_path}/${new_name}`;
            let new_img = path.resolve(`${new_img_text}`).replace(/\\/g, '/');

            createFolderAndImage(new_path, ori_path, new_img);

            async function createFolderAndImage(new_path, ori_path, new_img) {
                if(!fs.existsSync(new_path)) {
                    await mkdir(new_path, { recursive: true })
                }
        
                let oriPath = `./${ori_path}`;
                // await rename(oriPath, new_img);
                fs.copyFile(oriPath, new_img, (err) => {
                    if (err) throw err;
                    console.log('source was copied to destination');
                });
                await stat(new_img);
            }
            
        }

        res.status(200).json({
            status_code: 200,
            message: 'Your upload completed successfully'
        });

    } catch(error) {
        next(error);
    } 
}

const getWorkOrderByBarcode = async (req, res, next) => {
    // let barcode = 'K200701096443' ; 
    const { barcode } = req.body;
    let result = null;

    try { 
        
        let workOrder = await PvsWorkOrder.findOne({ 
            raw: true,
            nest: true,
            where: {
                barcode: barcode
            },
            include: [{
                model: PvsCustomer,
                as: 'customer',
                attributes: [
                    'pvs_customer_id',
                    [
                        sequelize.fn('AES_DECRYPT', sequelize.col('cus_name'), `${process.env.DB_SECRET}`),
                        'cus_name'
                    ],
                    [
                        sequelize.fn('AES_DECRYPT', sequelize.col('cus_lastname'), `${process.env.DB_SECRET}`),
                        'cus_lastname'
                    ]
                ],
                required: false // required: true forces an INNER JOIN, use required: false to force a LEFT JOIN.
            }]
        });

        // console.log(!isEmpty(workOrder)) ;
        // console.log(workOrder) ;

        if( workOrder != null) {

            let { sys_date, pvs_work_order_id, customer } = workOrder;
            let { pvs_customer_id, cus_name, cus_lastname } = customer;
            
            // result =  JSON.parse(JSON.stringify(workOrder));
            // console.log(result) ;

            check_cus_lastname = (cus_lastname != null ) ? cus_lastname : '';

            
            res.status(200).json({
                status_code: 200,
                message: 'success',
                data: {
                    sys_date: sys_date,
                    barcode: barcode,
                    customer_name: cus_name.toString('utf8'),
                    customer_lastname: check_cus_lastname.toString('utf8'),
                    pvs_work_order_id: pvs_work_order_id,
                    pvs_customer_id: pvs_customer_id
                },
            });
        } else {

            res.status(200).json({
                status_code: 204,
                message: 'ไม่มีงานตัวนี้ในระบบ'
            });

        }
        
    } catch(error) {
        next(error);
    }

}

const getIDCardForMessengers = async (req, res, next) => {

    try {   
        // save information for Card ID
        const jobs = await ApiDipchipMessenger.findAll({
            raw: true,
            nest: true,
            attributes: [
                [
                    sequelize.fn('AES_DECRYPT', sequelize.col('card_id'), `${process.env.DB_SECRET}`),
                    'card_id'
                ],
                [
                    sequelize.fn('AES_DECRYPT', sequelize.col('name_title'), `${process.env.DB_SECRET}`),
                    "name_title"
                ],
                [
                    sequelize.fn('AES_DECRYPT', sequelize.col('name'), `${process.env.DB_SECRET}`),
                    "name"
                ],
                [
                    sequelize.fn('AES_DECRYPT', sequelize.col('name_mid'), `${process.env.DB_SECRET}`),
                    "name_mid"
                ],
                [
                    sequelize.fn('AES_DECRYPT', sequelize.col('surname'), `${process.env.DB_SECRET}`),
                    "surname"
                ],
                [
                    sequelize.fn('AES_DECRYPT', sequelize.col('name_title_en'), `${process.env.DB_SECRET}`),
                    "name_title_en"
                ],
                [
                    sequelize.fn('AES_DECRYPT', sequelize.col('name_en'), `${process.env.DB_SECRET}`),
                    "name_en"
                ],
                [
                    sequelize.fn('AES_DECRYPT', sequelize.col('name_mid_en'), `${process.env.DB_SECRET}`),
                    "name_mid_en"
                ],
                [
                    sequelize.fn('AES_DECRYPT', sequelize.col('surname_en'), `${process.env.DB_SECRET}`),
                    "surname_en"
                ],
                'address_no',
                'address_moo',
                'address_sub_alley',
                'address_alley',
                'road',
                'sub_district',
                'district',
                'province',
                'sex',
                'date_of_birth',
                'location_of_birth',
                'date_of_issue',
                'date_of_expiry',
                'id_of_picture',
                'remark',
                'picture',
            ],
            order: [
                ['api_dipchip_messenger_id', 'DESC']
            ]
        });

        console.log(jobs) ;

        if(jobs.length > 0) {
            // let result =  JSON.parse(JSON.stringify(jobs));

            var result = [];

            for( i = 0 ; i<jobs.length; i++ ) {
                result[i] = jobs[i];
                let {  card_id, name_title, name, name_mid , surname, name_title_en, name_en, name_mid_en, surname_en } = jobs[i];
                // let { work_order_barcode, card_id, name} = jobs[i];

                  result[i].card_id = card_id.toString('utf8') ;
                  result[i].name_title = name_title.toString('utf8') ;
                  result[i].name = name.toString('utf8') ;
                  result[i].name_mid = name_mid.toString('utf8') ;
                  result[i].surname = surname.toString('utf8') ;
                  result[i].name_title_en = name_title_en.toString('utf8') ;
                  result[i].name_en = name_en.toString('utf8') ;
                  result[i].name_mid_en = name_mid_en.toString('utf8') ;
                  result[i].surname_en = surname_en.toString('utf8') ;
                //   console.log(card_id.toString('utf8'));
            }

            res.status(200).json({
                status_code: 200,
                message: 'success',
                data: result
            });
        }
        
    } catch(error) {
        next(error);
       
    }

}

const saveIDCardForMessenger = async (req, res, next) => {

    const { card_id, name_title, name, name_mid , surname, name_title_en, name_en, name_mid_en, 
            surname_en, address_no, address_moo, address_sub_alley, address_alley,road, sub_district,
            district, province, sex, date_of_birth, location_of_birth, date_of_issue, date_of_expiry, 
            id_of_picture, remark, picture } = req.body;
    const current_timestamp = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');
    let dipchip ;

    try {
        console.log("ID Card : ") ;
        console.log(card_id) ;
        //  Check ID Card In Database
        let dipchip = await ApiDipchipMessenger.findOne({
            where: {
                card_id: sequelize.fn('AES_ENCRYPT', card_id, `${process.env.DB_SECRET}`)
            }
        });

        // console.log(dipchip) ;
        
        // Check New ID Caed
        if(dipchip){

            console.log(dipchip) ;

            res.status(200).json({
                status_code: 200,
                message: 'มี ID Card นี้อยู่แล้ว',
            });

        }else{
             // save information for Card ID
            const newDipchipMessenger= await ApiDipchipMessenger.create({
                system_timestamp: current_timestamp,
                card_id: sequelize.fn('AES_ENCRYPT', card_id, `${process.env.DB_SECRET}`),
                name_title: sequelize.fn('AES_ENCRYPT', name_title, `${process.env.DB_SECRET}`),
                name: sequelize.fn('AES_ENCRYPT', name, `${process.env.DB_SECRET}`),
                name_mid:  sequelize.fn('AES_ENCRYPT', name_mid, `${process.env.DB_SECRET}`),
                surname: sequelize.fn('AES_ENCRYPT', surname, `${process.env.DB_SECRET}`), 
                name_title_en: sequelize.fn('AES_ENCRYPT', name_title_en, `${process.env.DB_SECRET}`), 
                name_en: sequelize.fn('AES_ENCRYPT', name_en, `${process.env.DB_SECRET}`),
                name_mid_en: sequelize.fn('AES_ENCRYPT', name_mid_en, `${process.env.DB_SECRET}`),
                surname_en: sequelize.fn('AES_ENCRYPT', surname_en, `${process.env.DB_SECRET}`), 
                address_no: address_no,
                address_moo: address_moo,
                address_sub_alley: address_sub_alley,
                address_alley: address_alley,
                road: road,
                sub_district: sub_district, 
                district: district,
                province: province, 
                sex: sex,
                date_of_birth: date_of_birth, 
                location_of_birth: location_of_birth,
                date_of_issue: date_of_issue,
                date_of_expiry: date_of_expiry,
                id_of_picture: id_of_picture,
                remark: remark,
                picture: picture
            });

            if(!(newDipchipMessenger == null)) {

                let { card_id, name } = newDipchipMessenger;

                res.status(200).json({
                    status_code: 200,
                    message: 'บันทึกข้อมูล New ID Card Messenger เรียบร้อยแล้ว',
                });
            }else{

                const error = new Error("การบันทึกข้อมูลผิดพลาด");
                error.statusCode = 500;
                throw error;

            }

        }

    } catch(error) {
        next(error);
       
    }
}

const savePhotoForMessenger = async (req, res, next) => {
    let error = null;
    let result_save = null;
    let new_img_text = '';
    
    try {
        const file = req.file;
        const { filename: ori_name, path: ori_path } = file;
        // ต้องการหาเลขที่ ID Card
        let raw_card_id = path.basename(ori_path, '.jpg');
        let card_id = raw_card_id.substring(raw_card_id.length - 13, raw_card_id.length);

        console.log('card_id : ' + card_id);

        if(file) { 
            // const { filename: ori_name, path: ori_path } = file;
            const ext = path.extname(file.originalname);
        
            let new_path = `./public/dipchip/messenger/${card_id}`;            
            let new_name = `${card_id}${ext}`;
            new_img_text = `${new_path}/${new_name}`;
            let new_img = path.resolve(`${new_img_text}`).replace(/\\/g, '/');
    
            if(!fs.existsSync(new_path)) {
                await mkdir(new_path, { recursive: true })
            }


    
            let oriPath = `./${ori_path}`;
            // await rename(oriPath, new_img);
            fs.copyFile(oriPath, new_img, (err) => {
                if (err) throw err;
                console.log('source was copied to destination');
            });
            await stat(new_img);
    
        }

        // save
        // let idcard_data = await ApiDipchipMessenger.findOne({ 
        //     where: {
        //         card_id: card_id
        //     }
        // });

        // if(!idcard_data) {
        //     result_save = await ApiDipchipMessenger.create({
        //         card_id: card_id,
        //         picture: String(new_img_text)
        //     });
        // } else {
        //      console.log('Update card_id : ' + card_id);
        //     result_save = await ApiDipchipMessenger.update({
        //         card_id: card_id,
        //         picture: String(new_img_text)
        //     },{
        //         where: {
        //             card_id: card_id
        //         }
        //     });
        // }

        res.status(200).json({
            status_code: 200,
            message: 'Your upload completed successfully'
        });

    } catch(error) {
        next(error);
    } 
}

const saveSignature = async (req, res, next) => {
    let error = null;
    let result_save = null;
    let new_img_text = '';
    
    try {
        const file = req.file;
        const {work_order_barcode} = req.body;
        const { filename: ori_name, path: ori_path } = file;
          
        // console.log(work_order_barcode);

        if(file) { 
            // const { filename: ori_name, path: ori_path } = file;
            const ext = path.extname(file.originalname);
        
            let new_path = `./public/dipchip/workorder/${work_order_barcode}/signature/`;            
            let new_name = `${work_order_barcode}${ext}`;
            new_img_text = `${new_path}/${new_name}`;
            let new_img = path.resolve(`${new_img_text}`).replace(/\\/g, '/');

            createFolderAndImage(new_path, ori_path, new_img);
    
            async function createFolderAndImage(new_path, ori_path, new_img) {
                if(!fs.existsSync(new_path)) {
                    await mkdir(new_path, { recursive: true })
                }
        
                let oriPath = `./${ori_path}`;
                // await rename(oriPath, new_img);
                fs.copyFile(oriPath, new_img, (err) => {
                    if (err) throw err;
                    console.log('source was copied to destination');
                });
                await stat(new_img);
            }
    
        }

        res.status(200).json({
            status_code: 200,
            message: 'Your upload completed successfully'
        });

    } catch(error) {
        next(error);
    } 
}

const getIDCardForMessenger = async (req, res, next) => {

    try {
        const { id_card } = req.query;
        // let id_card = '1700400039321'; 
        console.log(id_card);

        let filepath = `../public/dipchip/messenger/${id_card}/${id_card}.jpg`;
        let dir = path.join(__dirname, filepath);
    
        res.status(200).sendFile(dir);

    } catch(error) {
        next(error);
    } 
}


const saveLog = async (req, res, next) => {

    try {

        // console.log(req.body) ;
        const {work_order_barcode, work_order_id, customer_id, status_id, log_description} = req.body;
        const current_timestamp = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');

        const newLog = await ApiDipchipCardReaderLog.create({
            system_timestamp: current_timestamp,
            work_order_barcode: work_order_barcode, 
            work_order_id: work_order_id,
            customer_id: customer_id, 
            status_id: status_id,
            log_description: log_description
        });

        if(!(newLog == null)) {

            // let { card_id, name } = newLog;

            res.status(200).json({
                status_code: 200,
                message: 'บันทึกข้อมูล Log เรียบร้อยแล้ว',
            });
        }else{
            res.status(500).json({
                status_code: 500,
                message: "การบันทึกข้อมูลผิดพลาด"
            });
        }
    } catch(error) {
        next(error);
    } 
}


const getLog = async (req, res, next) => {
    
    try {

        const Logs = await ApiDipchipCardReaderLog.findAll({
            raw: true,
            nest: true,
            order: [
                ['api_dipchip_card_reader_log_id', 'DESC']
            ] 
        });

        if(Logs.length > 0) {

            res.status(200).json({
                status_code: 200,
                message: 'Your Get log success',
                data:{
                    Logs
                }
            });
        }

        

    } catch(error) {
        next(error);
    } 
}

const getLogByBarcode = async (req, res, next) => {

    try {


        const { work_order_barcode } = req.body;
        const log = await ApiDipchipCardReaderLog.findOne({
            raw: true,
            nest: true,
            limit: 1,
            where: {
                work_order_barcode: work_order_barcode
            },
            order: [ [ 'system_timestamp', 'DESC' ]]
        });

        console.log(log) ;

        if(!(log == null)) {

            const {api_dipchip_card_reader_log_id, system_timestamp, work_order_barcode, work_order_id, customer_id, status_id, log_description} = log;

            res.status(200).json({
                status_code: 200,
                data: {
                    api_dipchip_card_reader_log_id: api_dipchip_card_reader_log_id, 
                    system_timestamp: system_timestamp, 
                    work_order_barcode: work_order_barcode, 
                    work_order_id: work_order_id,
                    customer_id: customer_id, 
                    status_id: status_id,
                    log_description: log_description
                },
            });
        }else{

            res.status(500).json({
                status_code: 500,
                message: "ไม่ได้บันทึกงานนี้ ในระบบ E-KYC"
            });

        }
    } catch(error) {
        next(error);
    } 
}


module.exports = {
    getIDCardAll,
    saveIDCard,
    savePhoto,
    getWorkOrderByBarcode,
    getIDCardForMessengers,
    saveIDCardForMessenger,
    savePhotoForMessenger,
    saveSignature,
    getIDCardForMessenger,
    saveLog,
    getLog,
    getLogByBarcode
}