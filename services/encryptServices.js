const crypto = require('crypto');
const CryptoJS = require('crypto-js');

// var EncryptAESAES = (text) => {
//     const method = "aes-256-cbc";
//     const secret_key = 'pgutr52p';
//     const secret_iv = 'PivotCenterIV';

//     let encode = '';

//     let key = CryptoJS.SHA256(secret_key).toString().substr(0, 32);
//     let iv = CryptoJS.SHA256(secret_iv).toString().substr(0, 16);

//     var EncryptAESor = crypto.createCipheriv(method, key, iv);
//     var EncryptAES_data =  EncryptAESor.update(text, 'utf8', 'base64') + EncryptAESor.final('base64');

//     var cipher = CryptoJS.enc.Utf8.parse(EncryptAES_data+"::"+iv);
//     encode = CryptoJS.enc.Base64.stringify(cipher);
//     return encode;
// }

const EncryptAES = (function() {

    function EncryptAES(password) {
        this._method = "aes-256-cbc";
        this._secret_key = 'pgutr52p';
        this._secret_iv = 'PivotCenterIV';

        this._key = CryptoJS.SHA256(this._secret_key).toString().substr(0, 32);
        this._iv = CryptoJS.SHA256(this._secret_iv).toString().substr(0, 16);

        this._encoded = null;
        this._text = password;
    }

    EncryptAES.prototype.EncryptAES = function() {
        let EncryptAESor = crypto.createCipheriv(this._method, this._key, this._iv);
        let EncryptAES_data =  EncryptAESor.update(this._text, 'utf8', 'base64') + EncryptAESor.final('base64');
        let cipher = CryptoJS.enc.Utf8.parse(EncryptAES_data+"::"+this._iv);
        let encode = CryptoJS.enc.Base64.stringify(cipher);
        this._encoded = encode;
    }

    EncryptAES.prototype.setEncryptAES = function() {
         this.EncryptAES();
    }

    EncryptAES.prototype.getEncryptAES = function() {
        this.setEncryptAES();
        return this._encoded;
    }

    EncryptAES.prototype.comparePassword = function(cipher) {
        try {
            this.setEncryptAES();
            // console.log(this._encoded)

            if(this._encoded !== cipher) {
                throw new Error('รหัสผ่านไม่ตรงกัน');
            }
        } catch (err) {
            
            return { status: false, message: 'รหัสผ่านไม่ตรงกัน'};
        }
        
        return { status: true, message: 'รหัสผ่านตรงกัน'};
    }
    return EncryptAES;
})();

module.exports = EncryptAES

