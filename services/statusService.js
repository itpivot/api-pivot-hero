const db = require('../models');

const ApiMarketingStatus = db.api_marketing_status;

const sequelize = db.sequelize;


const getStatus = async (req, res, next) => {
    try {
        let status = await ApiMarketingStatus.findAll();

        res.status(200).json({
            status_code: 200,
            message: 'success',
            status: status
        });

    } catch (error) {
       next(error);
    }
}

module.exports = {
    getStatus
}