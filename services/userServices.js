const db = require('../models');

const User = db.user;
const Employee = db.employee;
const ApiMarketingUser = db.api_marketing_user;
const ApiMarketingRole = db.api_marketing_role;
const ApiMarketingCompanyMessengerUser = db.api_marketing_company_messenger_user;
const ApiMarketingCompany = db.api_marketing_company;
const sequelize = db.sequelize;

require('dotenv').config();

const getProfile = async (req, res, next) => {
    try {
        const { user } = req;

        const profile = await ApiMarketingUser.findOne({
            raw: true,
            nest: true,
            where: {
                marketing_user_id: user.marketing_user_id
            },
            include: [{
                model: User,
                as: 'user',
                required: false, // required: true forces an INNER JOIN, use required: false to force a LEFT JOIN.
                include: [{
                    model: Employee,
                    as: 'employee',
                    attributes: [
                        'code',
                        [
                            sequelize.fn('AES_DECRYPT', sequelize.col('firstname'), `${process.env.DB_SECRET}`),
                            'firstname'
                        ],
                        [
                            sequelize.fn('AES_DECRYPT', sequelize.col('lastname'), `${process.env.DB_SECRET}`),
                            'lastname'
                        ],
                        [
                            sequelize.fn('AES_DECRYPT', sequelize.col('telephone'), `${process.env.DB_SECRET}`),
                            'telephone'
                        ],
                    ],
                    required: false // required: true forces an INNER JOIN, use required: false to force a LEFT JOIN.
                }]
            },
            {
                attributes: ['role_name'],
                model: ApiMarketingRole,
                as: 'api_user_role',
            },
        ]
        });

        if(!profile) {
            const error = new Error("User Not Found.");
            error.statusCode = 500;
            throw error;
        }

        let { user: user_data, api_user_role } = profile;
        let emp = user_data.employee;
        let { role_name } = api_user_role;
        let { employee_id, code, firstname, lastname, telephone } = emp;

        res.status(200).json({
            status_code: 200,
            data: {
                code: code,
                firstname: firstname.toString('utf8'),
                lastname: lastname.toString('utf8'),
                telephone: telephone.toString('utf8'),
                role: role_name,
            },
        });
        
        // next();
    } catch(error) {
        next(error);
    }
}

module.exports = {
    getProfile
};