const db = require('../models');
const path = require('path');

const moment = require('moment');
const { Op } = require("sequelize");
const fs = require('fs');
const util = require('util');
const stat = util.promisify(fs.stat);
const rename = util.promisify(fs.rename);
const access = util.promisify(fs.access);
const mkdir = util.promisify(fs.mkdir);
const cp = util.promisify(fs.copyFile);

const writeFile = util.promisify(fs.writeFile)

// const ApiMarketingCompany                = db.api_marketing_company;
const ApiMarketingUser                   = db.api_marketing_user;
const ApiMarketingCompanyMessengerActive = db.api_marketing_company_messenger_active;
const ApiMarketingCompanyMessengerUser   = db.api_marketing_company_messenger_user;
const ApiMarketingWorkOrder              = db.api_marketing_work_order;
const ApiMarketingWorkOrderLocation      = db.api_marketing_work_order_location;
const ApiMarketingWorkOrderMile          = db.api_marketing_work_order_mile;
const ApiMarketingWorkOrderSignature     = db.api_marketing_work_order_signature;
const ApiMarketingSender                 = db.api_marketing_sender;
const ApiMarketingReceiver               = db.api_marketing_receiver;
const ApiMarketingStatus                 = db.api_marketing_status;
const ApiTypeWork                        = db.api_type_work;

const District                           = db.district;
const Amphure                            = db.amphure;
const Province                           = db.province;

const sequelize = db.sequelize;


const getOrders = async (req, res, next) => {
    const { user } = req;
    const { marketing_user_id } = user;
    let company_id     = null;
    let jobs           = [];
    let result         = [];

    const current_date = moment(Date.now()).format('YYYY-MM-DD'); 
    
    try {
        const profile = await ApiMarketingUser.findOne({
            where: { marketing_user_id: marketing_user_id },
            include: [{
                attributes: ['companyId'],
                model: ApiMarketingCompanyMessengerActive,
                as: 'user_active'
            },{
                attributes: ['companyId'],
                model: ApiMarketingCompanyMessengerUser,
                as: 'user_company'
            }]
        });

        company_id = profile.user_company.companyId || profile.user_active.companyId;
        if(!company_id) {
            const error = new Error("ไม่พบข้อมูลของแมสเซ็นเจอร์");
            error.statusCode = 500;
            throw error;
        }

        jobs = await ApiMarketingWorkOrder.findAll({
            where: { 
                companyId: company_id,
                date_meet: current_date,
                apiUserId: null,
                active: 1 
            },
            include: [{
                    attributes: ['sender_id', 'name', 'surname', 'address', 'department', 'telephone'],
                    model: ApiMarketingSender, as: 'sender',
                    required: false
                },{
                    model: ApiMarketingWorkOrderLocation, as: 'locations',
                    required: false,
                    include: [
                        {
                            attributes: ['receiver_id', 'name', 'surname', 'address', 'department', 'telephone'],
                            model: ApiMarketingReceiver, as: 'receiver',
                            required: false,
                            include: [{
                                attributes: ['name', 'zipcode'],
                                model: District, as: 'district'
                            },{
                                attributes: ['name'],
                                model: Amphure, as: 'amphure'
                            },{
                                attributes: ['name'],
                                model: Province, as: 'province'
                            }]
                        },{
                            model: ApiTypeWork,
                            as: 'type_work'
                        }
                    ],
                    order: ['api_work_order_location_id', 'AES']
            },{
                attributes: [ 'status_id', 'status_name' ],
                model: ApiMarketingStatus,
                as: 'status'
            }],
        });

        if(jobs.length > 0) {
            let resp =  JSON.parse(JSON.stringify(jobs));

            result = resp.reduce((order, job) => {
                let { locations, status, sender, barcode } = job;
                let locate = JSON.parse(JSON.stringify(locations));
                let { receiver, type_work, order_remark, detail_type_work } = locate[0];
                let { type_name } = JSON.parse(JSON.stringify(type_work));
                let { district, amphure, province } = JSON.parse(JSON.stringify(receiver));

                // Check Work Type
                let firstTwoDigit = barcode.slice(0, 2);
                let work_type = 'งานปกติ';
                if(firstTwoDigit == 'MR') {
                    work_type = 'งานรูทีน';
                }

                order.push({
                    work_order_id: job.work_order_id,
                    barcode: job.barcode,
                    date_order: moment(job.date_order).format('DD/MM/YYYY'),
                    date_meet: moment(job.date_meet).format('DD/MM/YYYY'),
                    active: job.active,
                    status_id: status.status_id,
                    status_name: status.status_name,
                    sender_name: `${sender.name} ${sender.surname}`,
                    sender_telephone: sender.telephone,
                    sender_address: sender.address,
                    receiver_name: `${receiver.name} ${receiver.surname}`,
                    telephone: receiver.telephone,
                    department: receiver.department,
                    type_name: type_name,
                    order_remark,
                    address: `${receiver.address} ${district.name} ${amphure.name} ${province.name} ${district.zipcode}`,
                    locations: locations.length,
                    work_type: work_type,
                    detail_type_work: (detail_type_work != null) ? detail_type_work : null,
                });
                return order;
            } , []);
        }

        res.status(200).json({
            status_code: 200,
            message: 'success',
            data: result
        });
    } catch(error) {
        next(error);
    }
};


const getMessengerOrder = async (req, res, next) => {
    const { user }              = req;
    const { marketing_user_id } = user;
    const current_date = moment(Date.now()).format('YYYY-MM-DD'); 

    let jobs                    = [];
    let result                  = [];

    try{
        jobs = await ApiMarketingWorkOrder.findAll({
            where: { 
                date_meet: current_date,
                active: 1,
                apiUserId: marketing_user_id,
                statusId: {
                    [Op.lt]: 3
                }
            },
            include: [{
                    attributes: ['sender_id', 'name', 'surname', 'address', 'department', 'telephone'],
                    model: ApiMarketingSender, as: 'sender',
                    required: false
                },{
                    model: ApiMarketingWorkOrderLocation, as: 'locations',
                    required: false,
                    include: [
                        {
                            attributes: ['receiver_id', 'name', 'surname', 'address', 'department', 'telephone'],
                            model: ApiMarketingReceiver, as: 'receiver',
                            required: false,
                            include: [{
                                attributes: ['name', 'zipcode'],
                                model: District, as: 'district'
                            },{
                                attributes: ['name'],
                                model: Amphure, as: 'amphure'
                            },{
                                attributes: ['name'],
                                model: Province, as: 'province'
                            }]
                        },{
                            model: ApiTypeWork,
                            as: 'type_work'
                        }
                ],
                order: ['api_work_order_location_id', 'AES']
            },{
                attributes: [ 'status_id', 'status_name' ],
                model: ApiMarketingStatus,
                as: 'status'
            }],
        });

        if(jobs.length > 0) {
            let resp =  JSON.parse(JSON.stringify(jobs));
    
            result = resp.reduce((order, job) => {
                let { locations, status, sender, barcode } = job;
                let locate = JSON.parse(JSON.stringify(locations));
                let { receiver, type_work, order_remark, detail_type_work } = locate[0];
                let { type_name } = JSON.parse(JSON.stringify(type_work));
                let { district, amphure, province } = JSON.parse(JSON.stringify(receiver));
                
                // Check Work Type
                let firstTwoDigit = barcode.slice(0, 2);
                let work_type = 'งานปกติ';
                if(firstTwoDigit == 'MR') {
                    work_type = 'งานรูทีน';
                }

                order.push({
                    work_order_id: job.work_order_id,
                    barcode: job.barcode,
                    date_order: moment(job.date_order).format('DD/MM/YYYY'),
                    date_meet: moment(job.date_meet).format('DD/MM/YYYY'),
                    active: job.active,
                    status_id: status.status_id,
                    status_name: status.status_name,
                    sender_name: `${sender.name} ${sender.surname}`,
                    sender_telephone: sender.telephone,
                    sender_address: sender.address,
                    receiver_name: `${receiver.name} ${receiver.surname}`,
                    telephone: receiver.telephone,
                    department: receiver.department,
                    type_name: type_name,
                    order_remark,
                    address: `${receiver.address} ${district.name} ${amphure.name} ${province.name} ${district.zipcode}`,
                    locations: locations.length,
                    work_type: work_type,
                    detail_type_work: (detail_type_work != null) ? detail_type_work : null,
                });
                return order;
            } , []);
        }
    
        res.status(200).json({
            status_code: 200,
            message: 'success',
            data: result
        });
    } catch(error) {
        next(error);
    }
}

const receiveJob = async (req, res, next) => {
    let transaction;
    let { user } = req;
    let { marketing_user_id: api_id } = user;
    let { order_id } = req.body;
    let current_timestamp = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');

    try {
        if(!order_id) {
            const error = new Error("ไม่พบงานนี้");
            error.statusCode = 500;
            throw error;
        }

        let order = await ApiMarketingWorkOrder.findOne({
            where: {
                work_order_id: order_id, 
                apiUserId: null
            }
        });

        if(!order) {
            const error = new Error("ไม่พบงานนี้ เนื่องจากมีผู้รับงานไปแล้ว");
            error.statusCode = 500;
            throw error;
        }

        // begin transaction
        transaction = await sequelize.transaction();
        await ApiMarketingWorkOrder.update({ 
            apiUserId: api_id, 
            statusId: 2, 
            date_receive: current_timestamp 
        }, { 
            where: { 
                work_order_id: order_id 
            }, 
            transaction 
        });

        // commit
        await transaction.commit();

        res.status(200).json({
            status_code: 200,
            message: 'success',
        });
    } catch(error) {
        if (transaction) await transaction.rollback();
        next(error);
    }
}

const returnJob = async (req, res, next) => {
    let transaction;
    let { user } = req;
    let { marketing_user_id: api_id } = user;
    const { order_id } = req.params;

    try {
        if(!order_id) {
            const error = new Error("ไม่พบงานนี้");
            error.statusCode = 500;
            throw error;
        }

        let order = await ApiMarketingWorkOrder.findOne({
            where: {
                apiUserId: api_id,
                work_order_id: order_id
            }
        });

        if(!order) {
            const error = new Error("ไม่พบงานนี้");
            error.statusCode = 500;
            throw error;
        }

        // begin transaction
        transaction = await sequelize.transaction();
        await ApiMarketingWorkOrder.update({ 
            apiUserId: null,
            statusId: 1,
            date_receive: null
        }, { where: { work_order_id: order_id }, transaction });

        // commit
        await transaction.commit();

        res.status(200).json({
            status_code: 200,
            message: 'success',
        });
    } catch (error) {
        if (transaction) await transaction.rollback();
        next(error);
    }
}

const getLocations = async (req, res, next) => {
    const { user } = req;
    const { marketing_user_id } = user;
    const { order_id } = req.params;
    let jobs              = [];
    let result            = [];

    try {
        jobs = await ApiMarketingWorkOrderLocation.findAll({
            where: { 
                workOrderId: order_id,
                active: 1
            },
            include: [
                {
                    attributes: [ 'status_id', 'status_name' ],
                    model: ApiMarketingStatus,
                    as: 'location_status'
                }, 
                {
                    model: ApiTypeWork,
                    as: 'type_work'
                },
                {
                    attributes: ['receiver_id', 'name', 'surname', 'address', 'department', 'telephone'],
                    model: ApiMarketingReceiver, as: 'receiver',
                    required: false,
                    include: [{
                        attributes: ['name', 'zipcode'],
                        model: District, as: 'district'
                    },{
                        attributes: ['name'],
                        model: Amphure, as: 'amphure'
                    },{
                        attributes: ['name'],
                        model: Province, as: 'province'
                    }]
            }],
        });    


        if(jobs.length > 0) {
            let resp =  JSON.parse(JSON.stringify(jobs));

            result = resp.reduce((order, job) => {
                let { receiver, location_status, type_work} = job;
                let { type_name } = JSON.parse(JSON.stringify(type_work));
                let { district, amphure, province } = JSON.parse(JSON.stringify(receiver));

                order.push({
                    work_order_location_id: job.work_order_location_id,
                    receiver_name: `${receiver.name} ${receiver.surname}`,
                    telephone: `${receiver.telephone}`,
                    address: `${receiver.address} ${district.name} ${amphure.name} ${province.name} ${district.zipcode}`,
                    barcode: job.barcode,
                    price: job.price,
                    type_name,
                    detail_type_work: job.detail_type_work,
                    status_id: location_status.status_id,
                    status_name: location_status.status_name,
                    order_remark: job.order_remark,
                    order_id: job.workOrderId
                });

                return order;
            }, []);
        }

        res.status(200).json({
            status_code: 200,
            message: 'success',
            data: result
        });

    } catch(error) {
        next(error);
    }
}


const getLatestMileNumber = async (req, res, next) => {
    const { user } = req;
    const { marketing_user_id } = user;
    const current_date = moment(Date.now()).format('YYYY-MM-DD');
    let result = [];

    try {
        result = await ApiMarketingWorkOrderMile.findOne({
            where: {
                apiUserId: marketing_user_id,
                system_timestamp: sequelize.where(sequelize.fn('date', sequelize.col('system_timestamp')), '=', `${current_date}`),
                active: 1
            },
            order: [['work_order_mile_id', 'DESC']],
            limit: 1
        });

        res.status(200).json({
            status_code: 200,
            message: 'success',
            data: result
        });
    } catch(error) {
        next(error);
    }

}


const uploadImage = async (req, res, next) => {
    const { user } = req;
    const { marketing_user_id } = user;
    const current_timestamp = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');
    const current_date = moment(Date.now()).format('YYYY-MM-DD');
    const { point, location_id , order_id, type, mile_number, latitude, longtitude } = req.body
    let error = null;
    let result_save = null;
    let jobs = [];
    let result = [];
    let new_img_text = '';
    
    try {
        const file = req.file;
        
        if(!file) {
            let old_img = '';

            let order = await ApiMarketingWorkOrder.findOne({
                where: {
                    work_order_id: order_id
                }
            });

            let mileOrder = await ApiMarketingWorkOrderMile.findOne({
                where: {
                    apiUserId: marketing_user_id,
                    system_timestamp: sequelize.where(sequelize.fn('date', sequelize.col('system_timestamp')), '=', `${current_date}`),
                    active: 1
                },
                order: [['work_order_mile_id', 'DESC']],
                limit: 1
            });

            let latestMile =  JSON.parse(JSON.stringify(mileOrder));

            if(latestMile.mile_number == mile_number) {
                old_img = latestMile.picture;
            }

            if(fs.existsSync(old_img)) {
                const ext = path.extname(old_img);

                if(!order) {
                    error = new Error("ไม่พบงานนี้");
                    error.statusCode = 500;
                    throw error;
                }

                let { barcode } = order;
                let new_path = `./public/uploads/${barcode}`;            
                let new_name = `${barcode}_${point}_${type}${ext}`;
                new_img_text = `${new_path}/${new_name}`;
                let new_img = path.resolve(`${new_img_text}`).replace(/\\/g, '/');

                if(!old_img) {
                    error = new Error("ไม่พบรูปภาพ");
                    error.statusCode = 500;
                    throw error;
                }

                if(!fs.existsSync(new_path)) {
                    await mkdir(new_path, { recursive: true })
                }

                await cp(old_img, new_img);
            } 
            
        } else {    
            const { filename: ori_name, path: ori_path } = file;
            const ext = path.extname(file.originalname);

            let order = await ApiMarketingWorkOrder.findOne({
                where: {
                    work_order_id: order_id
                }
            });
    
            let location = await ApiMarketingWorkOrderLocation.findOne({
                where: {
                    work_order_location_id: location_id,
                    workOrderId: order_id
                }
            });

            if(!order || !location) {
                error = new Error("ไม่พบงานนี้");
                error.statusCode = 500;
                throw error;
            }
            
            let { barcode } = order;
            let new_path = `./public/uploads/${barcode}`;            
            let new_name = `${barcode}_${point}_${type}${ext}`;
            new_img_text = `${new_path}/${new_name}`;
            let new_img = path.resolve(`${new_img_text}`).replace(/\\/g, '/');
    
            if(!fs.existsSync(new_path)) {
                await mkdir(new_path, { recursive: true })
            }
    
            let oriPath = `./${ori_path}`;

            await rename(oriPath, new_img);
            await stat(new_img);
    
        }

        // save
        let location_data = await ApiMarketingWorkOrderMile.findOne({ 
            where: {
                apiLocationId: location_id,
                location_type: type
            }
        });

        if(!location_data) {
            result_save = await ApiMarketingWorkOrderMile.create({
                apiLocationId: location_id,
                mile_number: mile_number,
                picture: String(new_img_text),
                latitude: latitude,
                longtitude: longtitude,
                active: 1,
                location_type: type,
                apiUserId: marketing_user_id
            });
        } else {
            result_save = await ApiMarketingWorkOrderMile.update({
                mile_number: mile_number,
                system_timestamp: current_timestamp,
                picture: String(new_img_text),
                latitude: latitude,
                longtitude: longtitude,
                active: 1,
                apiUserId: marketing_user_id
            },{
                where: {
                    work_order_mile_id: location_data.work_order_mile_id
                }
            });
        }


        if(result_save) {
            jobs = await ApiMarketingWorkOrderLocation.findOne({
                where: {
                    work_order_location_id: location_id
                },
                include: [
                    {
                        attributes: [ 'status_id', 'status_name' ],
                        model: ApiMarketingStatus,
                        as: 'location_status'
                    },
                    {
                        model: ApiMarketingWorkOrderMile,
                        as: 'miles',
                        required: false
                    }
                ]
            });

            if(!jobs) {
                const error = new Error("ไม่พบข้อมูล");
                error.statusCode = 500;
                throw error;
            }

            let resp =  JSON.parse(JSON.stringify(jobs));
            let { miles, location_status } = resp;
            
            result = {
                work_order_location_id: resp.work_order_location_id,
                order_id: resp.workOrderId,
                system_timestamp: resp.system_timestamp,
                order_id: resp.workOrderId,
                status_id: resp.statusId,
                barcode: resp.barcode,
                price: resp.price,
                status_name: location_status.status_name,
                remark: resp.messenger_remark,
            };

            if(!result['miles']) {
                result['miles'] = [];
            }

            if(miles) {
                // แยก start / end 
                miles = miles.map((mile) => {
                    mile['picture'] = mile['picture']+'?index='+Math.floor(Math.random() * 101); 
                    return { ...mile, picture: mile['picture'].replace('./public', '') };
                });

                let miles_data = miles.reduce((group_type, m) => {
                    if(!group_type['start']) {
                        group_type['start'] = [];
                    }
                    
                    if(!group_type['end']) {
                        group_type['end'] = [];
                    }
                    (group_type[m['location_type']] = group_type[m['location_type']] || []).push(m);
                    return group_type;
                }, {}); 
    
                result['miles'] = miles_data;
            }
            
           
        }

       

        
        
        res.status(200).json({
            status_code: 200,
            message: 'success',
            data: result ? [result] : []
        });
    } catch(error) {
        next(error);
    } 
}

const closeJob = async (req, res, next) => {
    const { user } = req;
    const { marketing_user_id } = user;
    const { status_id, remark, location_id, order_id } = req.body; 
    const current_timestamp = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');

    try {
        let result = await ApiMarketingWorkOrderLocation.update({
            messenger_remark: remark,
            statusId: status_id,
            date_closejob: current_timestamp,
            apiUserId: marketing_user_id
        },{
            where: {
                work_order_location_id: location_id
            }
        });

        let order_location = await ApiMarketingWorkOrderLocation.findAll({
            where: {
                workOrderId: order_id,
                active: 1
            }
        });

        if(order_location) {

                let closed = await order_location.filter((order) => { return (order['statusId'] == 3 || order['statusId'] == 4) });
                let status_id = 2;

                if(closed.length == order_location.length) { // ปิดงานครบแล้ว 

                    let reject_order = await order_location.filter((order) => { return (order['statusId'] == 4) });
                    status_id = (reject_order.length == order_location.length) ? 4 : 3;

                    await ApiMarketingWorkOrder.update({
                        statusId: status_id, 
                        date_closejob: current_timestamp
                    },{
                        where: {
                            work_order_id: order_id
                        }
                    });
                    
                } else {

                    await ApiMarketingWorkOrder.update({
                        statusId: status_id, 
                    },{
                        where: {
                            work_order_id: order_id
                        }
                    });

                }
        }
        

        res.status(200).json({
            status_code: 200,
            message: 'success',
        });
    } catch (error) {
        next(error);
    }
}

const searchOrders = async (req, res, next) => {
    const { user } = req;
    const { marketing_user_id } = user;
    const { keyword } = req.body; 
    const current_date = moment(Date.now()).format('YYYY-MM-DD');
    let company_id     = null;
    let jobs           = [];
    let result         = [];

    try {
        const profile = await ApiMarketingUser.findOne({
            where: { marketing_user_id: marketing_user_id },
            include: [{
                attributes: ['companyId'],
                model: ApiMarketingCompanyMessengerActive,
                as: 'user_active'
            },{
                attributes: ['companyId'],
                model: ApiMarketingCompanyMessengerUser,
                as: 'user_company'
            }]
        });

        company_id = profile.user_company.companyId || profile.user_active.companyId;
        if(!company_id) {
            const error = new Error("ไม่พบข้อมูลของแมสเซ็นเจอร์");
            error.statusCode = 500;
            throw error;
        }

        let str = (keyword != '') ? keyword.split(' '). join('') : '';

        if(str != '') {
            jobs = await ApiMarketingWorkOrder.findAll({
                where: {
                    apiUserId: null,
                    active: 1,
                    companyId: company_id,
                    date_meet: current_date,
                    [Op.or]: [
                        {
                            barcode: {
                                [Op.like]: `%${str}%`
                            }
                        },
                        {
                            '$locations.receiver.name$': {
                                [Op.like]: `%${str}%`
                            }
                        },
                        {
                            '$locations.receiver.surname$': {
                                [Op.like]: `%${str}%`
                            }
                        },
                        {
                            '$locations.receiver.telephone$': {
                                [Op.like]: `%${str}%`
                            }
                        }
                    ]
                },
                include: [{
                    attributes: ['sender_id', 'name', 'surname', 'address', 'department', 'telephone'],
                    model: ApiMarketingSender, as: 'sender',
                    required: false
                },{
                    model: ApiMarketingWorkOrderLocation, as: 'locations',
                    required: false,
                    include: [
                        {
                            attributes: ['receiver_id', 'name', 'surname', 'address', 'department', 'telephone'],
                            model: ApiMarketingReceiver, as: 'receiver',
                            required: false,
                            include: [{
                                attributes: ['name', 'zipcode'],
                                model: District, as: 'district'
                            },{
                                attributes: ['name'],
                                model: Amphure, as: 'amphure'
                            },{
                                attributes: ['name'],
                                model: Province, as: 'province'
                            }],
                        },{
                            model: ApiTypeWork,
                            as: 'type_work'
                        }
                    ],
                    order: ['api_work_order_location_id', 'AES']
                },{
                    attributes: [ 'status_id', 'status_name' ],
                    model: ApiMarketingStatus,
                    as: 'status'
                }],
            });
        } else {
            jobs = await ApiMarketingWorkOrder.findAll({
                where: {
                    apiUserId: null,
                    active: 1,
                    companyId: company_id,
                    date_meet: current_date,
                },
                include: [{
                    attributes: ['sender_id', 'name', 'surname', 'address', 'department', 'telephone'],
                    model: ApiMarketingSender, as: 'sender',
                    required: false
                },{
                    model: ApiMarketingWorkOrderLocation, as: 'locations',
                    required: false,
                    include: [
                        {
                            attributes: ['receiver_id', 'name', 'surname', 'address', 'department', 'telephone'],
                            model: ApiMarketingReceiver, as: 'receiver',
                            required: false,
                            include: [{
                                attributes: ['name', 'zipcode'],
                                model: District, as: 'district'
                            },{
                                attributes: ['name'],
                                model: Amphure, as: 'amphure'
                            },{
                                attributes: ['name'],
                                model: Province, as: 'province'
                            }],
                        },{
                            model: ApiTypeWork,
                            as: 'type_work'
                        }
                    ],
                    order: ['api_work_order_location_id', 'AES']
                },{
                    attributes: [ 'status_id', 'status_name' ],
                    model: ApiMarketingStatus,
                    as: 'status'
                }],
            });
        }

            if(jobs.length > 0) {
                let resp =  JSON.parse(JSON.stringify(jobs));
    
                result = resp.reduce((order, job) => {
                    let { locations, status, sender, barcode } = job;
                    let locate = JSON.parse(JSON.stringify(locations));
                    let { receiver, type_work, order_remark, detail_type_work } = locate[0];
                    let { type_name } = JSON.parse(JSON.stringify(type_work));
                    let { district, amphure, province } = JSON.parse(JSON.stringify(receiver));
                    
                    // Check Work Type
                    let firstTwoDigit = barcode.slice(0, 2);
                    let work_type = 'งานปกติ';
                    if(firstTwoDigit == 'MR') {
                        work_type = 'งานรูทีน';
                    }

                    order.push({
                        work_order_id: job.work_order_id,
                        barcode: job.barcode,
                        date_order: moment(job.date_order).format('DD/MM/YYYY'),
                        date_meet: moment(job.date_meet).format('DD/MM/YYYY'),
                        active: job.active,
                        status_id: status.status_id,
                        status_name: status.status_name,
                        sender_name: `${sender.name} ${sender.surname}`,
                        sender_telephone: sender.telephone,
                        sender_address: sender.address,
                        receiver_name: `${receiver.name} ${receiver.surname}`,
                        telephone: receiver.telephone,
                        department: receiver.department,
                        type_name: type_name,
                        order_remark,
                        address: `${receiver.address} ${district.name} ${amphure.name} ${province.name} ${district.zipcode}`,
                        locations: locations.length,
                        work_type: work_type,
                        detail_type_work: (detail_type_work != null) ? detail_type_work : null,
                    });
                    return order;
                } , []);

                
            }
        

        res.status(200).json({
            status_code: 200,
            message: 'success',
            data: result
        });
    } catch(error) {
        next(error);
    }
}

const searchMessengerOrders = async (req, res, next) => {
    const { user } = req;
    const { marketing_user_id } = user;
    const { keyword } = req.body; 
    const current_date = moment(Date.now()).format('YYYY-MM-DD');

    let str = (keyword != '') ? keyword.split(' '). join('') : '';

    let jobs                    = [];
    let result                  = [];

    try {
        if(str != '') {
            jobs = await ApiMarketingWorkOrder.findAll({
                where: { 
                    date_meet: current_date,
                    active: 1,
                    apiUserId: marketing_user_id,
                    statusId: {
                        [Op.lt]: 3
                    },
                    [Op.or]: [
                        {
                            barcode: {
                                [Op.like]: `%${str}%`
                            }
                        },
                        {
                            '$locations.receiver.name$': {
                                [Op.like]: `%${str}%`
                            }
                        },
                        {
                            '$locations.receiver.surname$': {
                                [Op.like]: `%${str}%`
                            }
                        },
                        {
                            '$locations.receiver.telephone$': {
                                [Op.like]: `%${str}%`
                            }
                        }
                    ]
                },
                include: [{
                    attributes: ['sender_id', 'name', 'surname', 'address', 'department', 'telephone'],
                    model: ApiMarketingSender, as: 'sender',
                    required: false
                },{
                    model: ApiMarketingWorkOrderLocation, as: 'locations',
                    required: false,
                    include: [
                        {
                            attributes: ['receiver_id', 'name', 'surname', 'address', 'department', 'telephone'],
                            model: ApiMarketingReceiver, as: 'receiver',
                            required: false,
                            include: [{
                                attributes: ['name', 'zipcode'],
                                model: District, as: 'district'
                            },{
                                attributes: ['name'],
                                model: Amphure, as: 'amphure'
                            },{
                                attributes: ['name'],
                                model: Province, as: 'province'
                            }]
                        },{
                            model: ApiTypeWork,
                            as: 'type_work'
                        }
                    ],
                    order: ['api_work_order_location_id', 'AES']
                },{
                    attributes: [ 'status_id', 'status_name' ],
                    model: ApiMarketingStatus,
                    as: 'status'
                }],
            });
        } else {
            jobs = await ApiMarketingWorkOrder.findAll({
                where: { 
                    date_meet: current_date,
                    active: 1,
                    apiUserId: marketing_user_id,
                    statusId: {
                        [Op.lt]: 3
                    },
                },
                include: [{
                    attributes: ['sender_id', 'name', 'surname', 'address', 'department', 'telephone'],
                    model: ApiMarketingSender, as: 'sender',
                    required: false
                },{
                    model: ApiMarketingWorkOrderLocation, as: 'locations',
                    required: false,
                    include: [
                        {
                            attributes: ['receiver_id', 'name', 'surname', 'address', 'department', 'telephone'],
                            model: ApiMarketingReceiver, as: 'receiver',
                            required: false,
                            include: [{
                                attributes: ['name', 'zipcode'],
                                model: District, as: 'district'
                            },{
                                attributes: ['name'],
                                model: Amphure, as: 'amphure'
                            },{
                                attributes: ['name'],
                                model: Province, as: 'province'
                            }]
                        },{
                            model: ApiTypeWork,
                            as: 'type_work'
                        }
                    ],
                    order: ['api_work_order_location_id', 'AES']
                },{
                    attributes: [ 'status_id', 'status_name' ],
                    model: ApiMarketingStatus,
                    as: 'status'
                }],
            });
        }
        

        if(jobs.length > 0) {
            let resp =  JSON.parse(JSON.stringify(jobs));

            result = resp.reduce((order, job) => {
                let { locations, status, sender, barcode } = job;
                let locate = JSON.parse(JSON.stringify(locations));
                let { receiver, type_work, order_remark, detail_type_work } = locate[0];
                let { type_name } = JSON.parse(JSON.stringify(type_work));
                let { district, amphure, province } = JSON.parse(JSON.stringify(receiver));

                // Check Work Type
                let firstTwoDigit = barcode.slice(0, 2);
                let work_type = 'งานปกติ';
                if(firstTwoDigit == 'MR') {
                    work_type = 'งานรูทีน';
                }

                order.push({
                    work_order_id: job.work_order_id,
                    barcode: job.barcode,
                    date_order: moment(job.date_order).format('DD/MM/YYYY'),
                    date_meet: moment(job.date_meet).format('DD/MM/YYYY'),
                    active: job.active,
                    status_id: status.status_id,
                    status_name: status.status_name,
                    sender_name: `${sender.name} ${sender.surname}`,
                    sender_telephone: sender.telephone,
                    sender_address: sender.address,
                    receiver_name: `${receiver.name} ${receiver.surname}`,
                    telephone: receiver.telephone,
                    department: receiver.department,
                    type_name: type_name,
                    order_remark,
                    address: `${receiver.address} ${district.name} ${amphure.name} ${province.name} ${district.zipcode}`,
                    locations: locations.length,
                    work_type: work_type,
                    detail_type_work: (detail_type_work != null) ? detail_type_work : null,
                });
                return order;
            } , []);
        }

        res.status(200).json({
            status_code: 200,
            message: 'success',
            data: result
        });
    } catch(error) {
        next(error);
    }
}

const summaryOrder  = async (req, res, next) => {
    const { user }              = req;
    const { marketing_user_id } = user;
    const current_date = moment(Date.now()).format('YYYY-MM-DD'); 

    let jobs                    = [];
    let result                  = [];

    try{
        jobs = await ApiMarketingWorkOrder.findAll({
            where: { 
                date_meet: current_date,
                active: 1,
                apiUserId: marketing_user_id,
                statusId: {
                    [Op.gt] : 2
                }
            },
            include: [{
                attributes: ['sender_id', 'name', 'surname', 'address', 'department', 'telephone'],
                model: ApiMarketingSender, as: 'sender',
                required: false
            },{
                model: ApiMarketingWorkOrderLocation, as: 'locations',
                required: false,
                include: [
                    {
                        attributes: ['receiver_id', 'name', 'surname', 'address', 'department', 'telephone'],
                        model: ApiMarketingReceiver, as: 'receiver',
                        required: false,
                        include: [{
                            attributes: ['name', 'zipcode'],
                            model: District, as: 'district'
                        },{
                            attributes: ['name'],
                            model: Amphure, as: 'amphure'
                        },{
                            attributes: ['name'],
                            model: Province, as: 'province'
                        }]
                    },{
                        model: ApiTypeWork,
                        as: 'type_work'
                    }
                ],
                order: ['api_work_order_location_id', 'AES']
            },{
                attributes: [ 'status_id', 'status_name' ],
                model: ApiMarketingStatus,
                as: 'status'
            }],
        });

        if(jobs.length > 0) {
            let resp =  JSON.parse(JSON.stringify(jobs));
    
            result = resp.reduce((order, job) => {
                let { locations, status, sender, barcode } = job;
                let locate = JSON.parse(JSON.stringify(locations));
                let { receiver, type_work, order_remark, detail_type_work } = locate[0];
                let { type_name } = JSON.parse(JSON.stringify(type_work));
                let { district, amphure, province } = JSON.parse(JSON.stringify(receiver));
                
                // Check Work Type
                let firstTwoDigit = barcode.slice(0, 2);
                let work_type = 'งานปกติ';
                if(firstTwoDigit == 'MR') {
                    work_type = 'งานรูทีน';
                }

                order.push({
                    work_order_id: job.work_order_id,
                    barcode: job.barcode,
                    date_order: moment(job.date_order).format('DD/MM/YYYY'),
                    date_meet: moment(job.date_meet).format('DD/MM/YYYY'),
                    active: job.active,
                    status_id: status.status_id,
                    status_name: status.status_name,
                    sender_name: `${sender.name} ${sender.surname}`,
                    sender_telephone: sender.telephone,
                    sender_address: sender.address,
                    receiver_name: `${receiver.name} ${receiver.surname}`,
                    telephone: receiver.telephone,
                    department: receiver.department,
                    type_name: type_name,
                    order_remark,
                    address: `${receiver.address} ${district.name} ${amphure.name} ${province.name} ${district.zipcode}`,
                    locations: locations.length,
                    work_type: work_type,
                    detail_type_work: (detail_type_work != null) ? detail_type_work : null,
                });
                return order;
            } , []);
        }
    
        res.status(200).json({
            status_code: 200,
            message: 'success',
            data: result
        });
    } catch(error) {
        next(error);
    }
}

const searchsummaryOrder = async (req, res, next) => {
    const { user } = req;
    const { marketing_user_id } = user;
    const { keyword } = req.body; 
    const current_date = moment(Date.now()).format('YYYY-MM-DD');

    let str = (keyword != '') ? keyword.split(' '). join('') : '';

    let jobs                    = [];
    let result                  = [];

    try {
        if(str != '') {
            jobs = await ApiMarketingWorkOrder.findAll({
                where: { 
                    date_meet: current_date,
                    active: 1,
                    apiUserId: marketing_user_id,
                    statusId: {
                        [Op.gt]: 2
                    },
                    [Op.or]: [
                        {
                            barcode: {
                                [Op.like]: `%${str}%`
                            }
                        },
                        {
                            '$locations.receiver.name$': {
                                [Op.like]: `%${str}%`
                            }
                        },
                        {
                            '$locations.receiver.surname$': {
                                [Op.like]: `%${str}%`
                            }
                        },
                        {
                            '$locations.receiver.telephone$': {
                                [Op.like]: `%${str}%`
                            }
                        }
                    ]
                },
                include: [{
                    attributes: ['sender_id', 'name', 'surname', 'address', 'department', 'telephone'],
                    model: ApiMarketingSender, as: 'sender',
                    required: false
                },{
                    model: ApiMarketingWorkOrderLocation, as: 'locations',
                    required: false,
                    include: [
                        {
                            attributes: ['receiver_id', 'name', 'surname', 'address', 'department', 'telephone'],
                            model: ApiMarketingReceiver, as: 'receiver',
                            required: false,
                            include: [{
                                attributes: ['name', 'zipcode'],
                                model: District, as: 'district'
                            },{
                                attributes: ['name'],
                                model: Amphure, as: 'amphure'
                            },{
                                attributes: ['name'],
                                model: Province, as: 'province'
                            }]
                        },{
                            model: ApiTypeWork,
                            as: 'type_work'
                        }
                    ],
                    order: ['api_work_order_location_id', 'AES']
                },{
                    attributes: [ 'status_id', 'status_name' ],
                    model: ApiMarketingStatus,
                    as: 'status'
                }],
            });
        } else {
            jobs = await ApiMarketingWorkOrder.findAll({
                where: { 
                    date_meet: current_date,
                    active: 1,
                    apiUserId: marketing_user_id,
                    statusId: {
                        [Op.gt]: 2
                    },
                },
                include: [{
                    attributes: ['sender_id', 'name', 'surname', 'address', 'department', 'telephone'],
                    model: ApiMarketingSender, as: 'sender',
                    required: false
                },{
                    model: ApiMarketingWorkOrderLocation, as: 'locations',
                    required: false,
                    include: [
                        {
                            attributes: ['receiver_id', 'name', 'surname', 'address', 'department', 'telephone'],
                            model: ApiMarketingReceiver, as: 'receiver',
                            required: false,
                            include: [{
                                attributes: ['name', 'zipcode'],
                                model: District, as: 'district'
                            },{
                                attributes: ['name'],
                                model: Amphure, as: 'amphure'
                            },{
                                attributes: ['name'],
                                model: Province, as: 'province'
                            }]
                        },{
                            model: ApiTypeWork,
                            as: 'type_work'
                        }
                    ],
                    order: ['api_work_order_location_id', 'AES']
                },{
                    attributes: [ 'status_id', 'status_name' ],
                    model: ApiMarketingStatus,
                    as: 'status'
                }],
            });
        }
        

        if(jobs.length > 0) {
            let resp =  JSON.parse(JSON.stringify(jobs));

            result = resp.reduce((order, job) => {
                let { locations, status, sender, barcode } = job;
                let locate = JSON.parse(JSON.stringify(locations));
                let { receiver, type_work, order_remark, detail_type_work } = locate[0];
                let { type_name } = JSON.parse(JSON.stringify(type_work));
                let { district, amphure, province } = JSON.parse(JSON.stringify(receiver));

                // Check Work Type
                let firstTwoDigit = barcode.slice(0, 2);
                let work_type = 'งานปกติ';
                if(firstTwoDigit == 'MR') {
                    work_type = 'งานรูทีน';
                }

                order.push({
                    work_order_id: job.work_order_id,
                    barcode: job.barcode,
                    date_order: moment(job.date_order).format('DD/MM/YYYY'),
                    date_meet: moment(job.date_meet).format('DD/MM/YYYY'),
                    active: job.active,
                    status_id: status.status_id,
                    status_name: status.status_name,
                    sender_name: `${sender.name} ${sender.surname}`,
                    sender_telephone: sender.telephone,
                    sender_address: sender.address,
                    receiver_name: `${receiver.name} ${receiver.surname}`,
                    telephone: receiver.telephone,
                    department: receiver.department,
                    type_name: type_name,
                    order_remark,
                    address: `${receiver.address} ${district.name} ${amphure.name} ${province.name} ${district.zipcode}`,
                    locations: locations.length,
                    work_type: work_type,
                    detail_type_work: (detail_type_work != null) ? detail_type_work : null,
                });
                return order;
            } , []);
        }

        res.status(200).json({
            status_code: 200,
            message: 'success',
            data: result
        });
    } catch(error) {
        next(error);
    }
}

const getDetailOrder = async (req, res, next) => {
    const { user }              = req;
    const { marketing_user_id } = user;
    const { location_id } = req.params;
    const current_date = moment(Date.now()).format('YYYY-MM-DD'); 

    let jobs              = [];
    let signature_data    = [];
    let result            = [];
    let error             = null;
    let sign_img          = '';

    try {
        jobs = await ApiMarketingWorkOrderLocation.findOne({
            where: {
                work_order_location_id: location_id
            },
            include: [
                {
                    attributes: [ 'status_id', 'status_name' ],
                    model: ApiMarketingStatus,
                    as: 'location_status'
                },
                {
                    model: ApiMarketingWorkOrderMile,
                    as: 'miles',
                    required: false
                }
            ]
        });

       
        if(!jobs) {
            const error = new Error("ไม่พบข้อมูล");
            error.statusCode = 500;
            throw error;
        }

        let resp =  JSON.parse(JSON.stringify(jobs));
        let { miles, location_status } = resp;

        signature_data = await ApiMarketingWorkOrderSignature.findOne({ 
            where: {
                apiLocationId: location_id,
            }
        });

        if(signature_data) {
            let sign_data = JSON.parse(JSON.stringify(signature_data));
            let { picture } = sign_data;
            sign_img = (picture != "" ? picture.replace('./public', '') : '');
        }
        
        result = {
            work_order_location_id: resp.work_order_location_id,
            order_id: resp.workOrderId,
            system_timestamp: resp.system_timestamp,
            order_id: resp.workOrderId,
            status_id: resp.statusId,
            barcode: resp.barcode,
            price: resp.price,
            status_name: location_status.status_name,
            remark: resp.messenger_remark,
            signature: sign_img
        };

        if(!result['miles']) {
            result['miles'] = [];
        }

        if(miles) {
            // แยก start / end 
            miles = miles.map((mile) => {
                mile['picture'] = mile['picture']+'?index='+Math.floor(Math.random() * 101); 
                return { ...mile, picture: mile['picture'].replace('./public', '') };
            });

            let miles_data = miles.reduce((group_type, m) => {
                if(!group_type['start']) {
                    group_type['start'] = [];
                }
                
                if(!group_type['end']) {
                    group_type['end'] = [];
                }
                
                (group_type[m['location_type']] = group_type[m['location_type']] || []).push(m);
                return group_type;
            }, {}); 

            result['miles'] = miles_data;
        }
        
        res.status(200).json({
            status_code: 200,
            message: 'success',
            data: result ? [result] : []
        });
    } catch(error) {
        next(error);
    }
}


const uploadSignature = async (req, res, next) => {
    const { user } = req;
    const { marketing_user_id } = user;
    const { point, location_id, order_id } = req.body; 
    const current_timestamp = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');
    const current_date      = moment(Date.now()).format('YYYY-MM-DD'); 
    let base64Image;

    let error = null;
    let result_save = null;
    let jobs = [];
    let result = [];

    try {
        const data = req.body.signature;
        if(data) {

            let order = await ApiMarketingWorkOrder.findOne({
                where: {
                    work_order_id: order_id
                }
            });

            let mileOrder = await ApiMarketingWorkOrderMile.findOne({
                where: {
                    apiUserId: marketing_user_id,
                    system_timestamp: sequelize.where(sequelize.fn('date', sequelize.col('system_timestamp')), '=', `${current_date}`),
                    active: 1
                },
                order: [['work_order_mile_id', 'DESC']],
                limit: 1
            });

            if(!order) {
                error = new Error("ไม่พบงานนี้");
                error.statusCode = 500;
                throw error;
            }

            let { barcode } = order;
            let new_path = `./public/signatures/${barcode}`; 
            let new_name = `${barcode}_${point}.jpg`; // MM202105240001_1.jpg / MM202105240001_2.jpg 
            let new_img_text = `${new_path}/${new_name}`;
            let new_img = path.resolve(`${new_img_text}`).replace(/\\/g, '/');
            
            if(!fs.existsSync(new_path)) {
                await mkdir(new_path, { recursive: true });
            }

            base64Image = data.split(';base64,').pop();
            await writeFile(new_img, base64Image, { encoding: 'base64' });

            // save - data 
            let signature_data = await ApiMarketingWorkOrderSignature.findOne({ 
                where: {
                    apiLocationId: location_id,
                }
            });

            if(!signature_data) {
                result_save = await ApiMarketingWorkOrderSignature.create({
                    apiLocationId: location_id,
                    picture: String(new_img_text),
                    apiUserId: marketing_user_id
                });
            } else {
                result_save = await ApiMarketingWorkOrderSignature.update({
                    system_timestamp: current_timestamp,
                    picture: String(new_img_text),
                    apiUserId: marketing_user_id
                },{
                    where: {
                        work_order_signature_id: signature_data.work_order_signature_id
                    }
                });
            }

            if(result_save) {
                new_img_text = new_img_text.replace('./public', '');
                res.status(200).json({
                    status_code: 200,
                    message: 'success',
                    signature: new_img_text,
                });
            }
        }
    } catch(error) {
        next(error);
    }
}

module.exports = {
    getOrders,
    getMessengerOrder,
    receiveJob,
    returnJob,
    getLocations,
    getLatestMileNumber,
    uploadImage,
    uploadSignature,
    closeJob,
    searchOrders,
    searchMessengerOrders,
    summaryOrder,
    searchsummaryOrder,
    getDetailOrder
}