const { QueryTypes } = require('sequelize');
const db = require('../models/index');


exports.index = async (req, res, next) => {

    try {

        const { user } = req;
        const { marketing_user_id } = user;
        // console.log(user) ;
        
        const result = await db.sequelize.query(
        `
            SELECT
            DATE_FORMAT(xx.date_closejob, "%d %b") as date_closejob,
            COUNT(xx.location_id) as sum_work_order,
            SUM(xx.mile) as sum_mile,
            CASE 
                WHEN (SUM(xx.mile) - mcc.minimum_kilometer) < 0 THEN 0
                ELSE SUM(xx.mile) - mcc.minimum_kilometer
            END as over_mile
            FROM(
                SELECT
                    x.id,
                    x.api_marketing_company_id,
                    x.date_closejob,
                    x.location_id,
                    x.mile_number,
                    x.prev_mile_number,
                    x.mile_number - x.prev_mile_number as mile
                FROM (
                        SELECT
                            wo.api_marketing_work_order_id as id ,
                            wo.api_marketing_company_id,
                            DATE_FORMAT( wo.date_closejob, '%Y-%m-%d') as date_closejob,
                            wol.api_marketing_work_order_location_id as location_id,
                            wol.detail_type_work as type,
                            wom.api_marketing_work_order_mile_id as mile_id,
                            y.mile_number ,
                            wom.mile_number as prev_mile_number
                        FROM api_marketing_work_order as wo
                        JOIN api_marketing_work_order_location as wol USING (api_marketing_work_order_id )
                        JOIN api_marketing_work_order_mile as wom USING (api_marketing_work_order_location_id)
                        CROSS JOIN (
                            SELECT  DATE_FORMAT( CONCAT( zzz.month_previous, "-20"), '%Y-%m-%d') as con_month_previous
                            FROM(
                                SELECT DATE_FORMAT( DATE_ADD(CURDATE(), INTERVAL zz.select_month MONTH), "%Y-%m" ) as month_previous
                                FROM(
                                    SELECT CASE WHEN z.today >= 21 THEN 0 ELSE -1 END as select_month
                                    FROM(
                                        SELECT DATE_FORMAT( CURDATE() , "%d" ) as today
                                    )z
                                )zz
                            )zzz
                        )zzzz
                        LEFT JOIN(
                            SELECT 
                                wol.api_marketing_work_order_location_id,
                                wom.mile_number
                            FROM api_marketing_work_order as wo
                            JOIN api_marketing_work_order_location as wol USING (api_marketing_work_order_id )
                            JOIN api_marketing_work_order_mile as wom USING (api_marketing_work_order_location_id)
                            where wom.api_marketing_work_order_mile_id > ( 
                                select min(womx.api_marketing_work_order_mile_id) 
                                FROM api_marketing_work_order as wox
                                JOIN api_marketing_work_order_location as wolx USING (api_marketing_work_order_id )
                                JOIN api_marketing_work_order_mile as womx USING (api_marketing_work_order_location_id)
                                where wolx.api_marketing_work_order_location_id = wol.api_marketing_work_order_location_id
                                and wo.api_marketing_work_order_id = wox.api_marketing_work_order_id
                            )
                        )y on y.api_marketing_work_order_location_id = wol.api_marketing_work_order_location_id
                        WHERE wom.api_marketing_work_order_mile_id < ( 
                            select max(womx.api_marketing_work_order_mile_id) 
                            FROM api_marketing_work_order as wox
                            JOIN api_marketing_work_order_location as wolx USING (api_marketing_work_order_id )
                            JOIN api_marketing_work_order_mile as womx USING (api_marketing_work_order_location_id)
                            where wolx.api_marketing_work_order_location_id = wol.api_marketing_work_order_location_id
                            and wo.api_marketing_work_order_id = wox.api_marketing_work_order_id
                        )
                        AND wo.api_marketing_user_id = ?
                        AND  DATE_FORMAT( wo.date_closejob, "%Y-%m-%d" )  >= zzzz.con_month_previous
                        AND  DATE_FORMAT( wo.date_closejob, "%Y-%m-%d" ) <= CURDATE()
                        AND wol.api_marketing_status_id = 3
                        AND wo.active = 1
                        ORDER BY wom.api_marketing_work_order_mile_id DESC
                )x
                WHERE  x.prev_mile_number is not null
            )xx
            JOIN api_marketing_company_cost as mcc ON mcc.api_marketing_company_id = xx.api_marketing_company_id
            GROUP BY xx.date_closejob        
        `, {
            replacements: [ marketing_user_id ],
            type: QueryTypes.SELECT
        });

        if(!result){
            throw new Error('ไม่พบข้อมูลรายงาน');
        }
        // กำหนดค่า Total
        let total = {
            total_work_order : 0 ,
            total_mile : 0,
            total_over_mile : 0
        } ;

        result.forEach(function(item, index, array) {
            total['total_work_order'] = total['total_work_order'] + Number(item.sum_work_order) ;
            total['total_mile'] = total['total_mile'] + item.sum_mile ;
            total['total_over_mile'] = total['total_over_mile'] + item.over_mile ;
            // console.log(item, index)
            // console.log(item.sum_mile) ;
        });

        const result_info = await db.sequelize.query(
            `
                SELECT  
                    CONCAT( "Report:", DATE_FORMAT( CURDATE(), ' %M %Y') ) as title ,
                    CONCAT( "21", DATE_FORMAT( zzz.month_previous , ' %M'), " - ", DATE_FORMAT( CURDATE(), '%d %M %Y') ) as sub_title
                FROM(
                    SELECT DATE_FORMAT( DATE_ADD(CURDATE(), INTERVAL zz.select_month MONTH), "%Y-%m-%d" ) as month_previous
                    FROM(
                        SELECT CASE WHEN z.today >= 21 THEN 0 ELSE -1 END as select_month
                        FROM(
                            SELECT DATE_FORMAT( CURDATE() , "%d" ) as today
                        )z
                    )zz
                )zzz        
            `, {
                type: QueryTypes.SELECT
            });
    
            if(!result_info){
                throw new Error('ไม่พบข้อมูลหัวข้อรายงาน');
            }

        // let info = {
        //     title : "รายงาน ธันวาคม 2563" ,
        //     sub_title : "20 พฤศจิกายน - 28 ธันวาคม 2563"
        // } ;

        // console.log(total) ;

        // for(index in result){
        //     // console.log(result[index]);
        //     let id = result[index]['id'] ;
        //     let location_id = result[index]['location_id'] ;
        //     let mile_id = result[index]['mile_id'] ;
        //     let mile_number = result[index]['mile_number'] ;

        //     arr[id] = mile_number;

        //     // console.log(mile_id);

        // }

        res.status(200).json({
            data: result,
            total: total,
            info: result_info
        });

    } catch (error) {
        res.status(400).json({
            error:{
                message: 'เกิดข้อผิดพลาด ' + error.message,
            } 
        });
    }

}
