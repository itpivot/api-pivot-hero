const userServices = require('../services/userServices');


const getProfile = async (req, res, next) => {
    return await userServices.getProfile(req, res, next);
}



module.exports = {
    getProfile
}