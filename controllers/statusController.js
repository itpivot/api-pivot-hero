const statusServices = require('../services/statusService');

const getStatus = async (req, res, next) => {
    return await statusServices.getStatus(req, res, next);
}

module.exports = {
    getStatus
}