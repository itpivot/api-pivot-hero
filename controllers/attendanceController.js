const AttendanceService = require('../services/attendanceServices');

const getAttendance = async(req, res, next) => {
    return await AttendanceService.getAttendance(req, res, next);
}

const checkIn = async(req, res, next) => {
    return await AttendanceService.checkIn(req, res, next);
}

const checkOut = async(req, res, next) => {
    return await AttendanceService.checkOut(req, res, next);
}

module.exports = {
    getAttendance,
    checkIn,
    checkOut
}