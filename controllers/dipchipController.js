const DipchipService = require('../services/dipchipServices');

// Service For Customer

const getIDCardAll = async(req, res, next) => {
    return await DipchipService.getIDCardAll(req, res, next);
}

const saveIDCard = async(req, res, next) => {
    return await DipchipService.saveIDCard(req, res, next);
}

const savePhoto = async (req, res, next) => {
    return await DipchipService.savePhoto(req, res, next);
}

const getWorkOrderByBarcode = async (req, res, next) => {
    return await DipchipService.getWorkOrderByBarcode(req, res, next);
}

// Save Image Signature 

const saveSignature = async (req, res, next) => {
    return await DipchipService.saveSignature(req, res, next);
}

// Services For Messenger

const getIDCardForMessengers = async(req, res, next) => {
    return await DipchipService.getIDCardForMessengers(req, res, next);
}

const saveIDCardForMessenger = async(req, res, next) => {
    return await DipchipService.saveIDCardForMessenger(req, res, next);
}

const savePhotoForMessenger = async (req, res, next) => {
    return await DipchipService.savePhotoForMessenger(req, res, next);
}

const getIDCardForMessenger = async (req, res, next) => {
    return await DipchipService.getIDCardForMessenger(req, res, next);
}

const getLog = async (req, res, next) => {
    return await DipchipService.getLog(req, res, next);
}

const saveLog = async (req, res, next) => {
    return await DipchipService.saveLog(req, res, next);
}

const getLogByBarcode = async (req, res, next) => {
    return await DipchipService.getLogByBarcode(req, res, next);
}



module.exports = {
    getIDCardAll,
    saveIDCard,
    savePhoto,
    getWorkOrderByBarcode,
    getIDCardForMessengers,
    saveIDCardForMessenger,
    savePhotoForMessenger,
    saveSignature,
    getIDCardForMessenger,
    getLog,
    saveLog,
    getLogByBarcode
}