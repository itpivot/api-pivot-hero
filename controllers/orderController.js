const orderServices = require('../services/orderService');


const getOrders = async (req, res, next) => {
    return await orderServices.getOrders(req, res, next);
}

const getMessengerOrder = async (req, res, next) => {
    return await orderServices.getMessengerOrder(req, res, next);
}

const receiveJob = async (req, res, next) => {
    return await orderServices.receiveJob(req, res, next);
}

const returnJob = async (req, res, next) => {
    return await orderServices.returnJob(req, res, next);
}

const getLocations = async (req, res, next) => {
    return await orderServices.getLocations(req, res, next);
}

const getLatestMileNumber = async (req, res, next) => {
    return await orderServices.getLatestMileNumber(req, res, next);
}

const uploadImage = async (req, res, next) => {
    return await orderServices.uploadImage(req, res, next);
}

const uploadSignature = async (req, res, next) => {
    return await orderServices.uploadSignature(req, res, next);
}

const closeJob = async (req, res, next) => {
    return await orderServices.closeJob(req, res, next);
}

const searchOrders = async (req, res, next) => {
    return await orderServices.searchOrders(req, res, next);
}

const searchMessengerOrders = async (req, res, next) => {
    return await orderServices.searchMessengerOrders(req, res, next);
}

const summaryOrder = async (req, res, next) => {
    return await orderServices.summaryOrder(req, res, next);
}

const searchsummaryOrder = async (req, res, next) => {
    return await orderServices.searchsummaryOrder(req, res, next);
}

const getDetailOrder = async (req, res, next) => {
    return await orderServices.getDetailOrder(req, res, next);
}

module.exports = {
    getOrders,
    getMessengerOrder,
    receiveJob,
    returnJob,
    getLocations,
    getLatestMileNumber,
    uploadImage,
    uploadSignature,
    closeJob,
    searchOrders,
    searchMessengerOrders,
    summaryOrder,
    searchsummaryOrder,
    getDetailOrder
}