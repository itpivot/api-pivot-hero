const AuthService = require('../services/authServices');

const signIn =  async (req, res, next) => {
    return await AuthService.signIn(req, res, next);
}

const signOut = async (req, res, next) => {
    return await AuthService.signOut(req, res, next);
}

module.exports = { 
    signIn,
    signOut
}