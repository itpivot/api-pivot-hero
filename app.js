const express = require('express');
const path = require('path');
const cors = require('cors');
const passport = require('passport');
const hetmet = require('helmet');
const rateLimit = require('express-rate-limit');
const cookieParser = require('cookie-parser');
const authMiddleware  = require('./middlewares/authMiddleware');

const authRouter = require('./routes/auth');
const userRouter = require('./routes/user');
const attendanceRouter = require('./routes/attendance');
const orderRouter = require('./routes/order');
const reportRouter = require('./routes/report');
const statusRouter = require('./routes/status')

// สำหรับ Project Dip Chip Card Reader (Demo) 
const dipchipRouter = require('./routes/dipchip');

// const {  } = require('./middlewares/errorHandle');
const handleError = require('./middlewares/errorHandle');

require('dotenv').config();

const app = express();

const limiter = rateLimit({
  windowMs: 10 * 1000, // 1 hrs in milliseconds (60 * 60 * 1000)
  max: 10 // 10 request in 10sec
});

app.use(cors());
app.use(limiter);
app.use(hetmet());

app.use(express.json());
app.use(cookieParser());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public'))); // Set static folder
app.use(passport.initialize());

app.use('/auth', authRouter);
app.use('/user', [ authMiddleware.verifyToken ,authMiddleware.isLogin ], userRouter);
app.use('/attendance', [ authMiddleware.verifyToken ,authMiddleware.isLogin ], attendanceRouter);
app.use('/order', [ authMiddleware.verifyToken ,authMiddleware.isLogin ], orderRouter);
app.use('/report', reportRouter);
app.use('/status', statusRouter);
// สำหรับ Project Dip Chip Card Reader (Demo) 
app.use('/dipchip', dipchipRouter);
// Error Handler : Place your error handler after all other middlewares
app.use(handleError); 

module.exports = app;

// const PORT = process.env.PORT || 3000
// app.listen(PORT, () => {
//     console.log(`Server Running on Port: ${PORT}`);
// });