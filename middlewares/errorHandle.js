module.exports = (err, req, res , next) => {

    const statusCode = err.statusCode || 500 ;

    return res.status(statusCode).json({
      error : {
        status_code: statusCode ,
        message : err.message,
        validation: err.validation
      }
    })
}

// const handleError = (error, req, res, next) => {
//     let { statusCode, message } = error;
//     res.status(statusCode).json({
//         error: {
//             status: statusCode || 500,
//             message: message
//         }
//     });
// }

// module.exports = {
//     handleError
// }