const express = require('express');
const passport = require('passport');
const db = require('../models');
const jwtConfig = require('../config/jwtConfig')
const jwt = require('jsonwebtoken');
const { Op } = require("sequelize");

const User = db.user;
const ApiMarketingUser = db.api_marketing_user;
const ApiMarketingRole = db.api_marketing_role;

const router = express.Router();

function isAuthorized(req, res, next) {
    passport.authenticate('jwt', { session: false } , async (err, token) => {
        if(err || !token) {
            res.status(401).json({
                error: {
                    status: 401,
                    message: 'Unauthorized.'
                }
            });
        } else {
            try {
                const userData = await ApiMarketingUser.findOne({
                    include: [{
                        model: User,
                        where: { username: token.username },
                        as: 'user'
                    }]
                });
                req.user = userData;
            } catch (error) {
                next(error);
            }
            next();
        }
    })(req, res, next); 
}

function verifyToken(req, res, next) {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];

    try {
        if(!authHeader && !token) {
            const error = new Error("No token provided.");
            error.statusCode = 403;
            throw error;
        }

        jwt.verify(token, jwtConfig.secret, async (err, decoded) => {
            try {
                if (err || !decoded) {
                    const error = new Error("Unauthorized.");
                    error.statusCode = 401;
                    throw error;
                }
    
                let expires = decoded.exp;
                if(Math.floor(Date.now() / 1000) >= expires) {
                    const error = new Error("Session Expire Please Login Again.");
                    error.statusCode = 401;
                    throw error;
                } else {
                    const userData = await ApiMarketingUser.findOne({
                        where: { 
                            apiRoleId: {
                                [Op.or]: [1, 3]
                            } 
                        }, // 2M && Rescue
                        include: [{
                            model: User,
                            where: { username: decoded.username },
                            as: 'user'
                        },{
                            attributes: ['role_name'],
                            model: ApiMarketingRole,
                            required: false,
                            as: 'api_user_role',
                        }]
                    });
        
                    if(!userData.token && !userData.expires) {
                        const error = new Error("Unauthorized.");
                        error.statusCode = 401;
                        throw error;
                    } else {
                        req.user = userData;
                       
                    }   
                    next();
                }
            } catch(error) {
                next(error);
            }
            
        });
    } catch(error) {
        next(error);
    }
}

async function refreshToken(req, res, next) {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];

    const { user } = req.user; 
    
    if(!user) {
        const error = new Error("Unauthorized.");
        error.statusCode = 401;
        throw error;
    }


    const userData = await ApiMarketingUser.findOne({
        where: { 
            apiRoleId: {
                [Op.or]: [1, 3]
            } 
         }, // 2M
        include: [{
            model: User,
            where: { username: user.username },
            as: 'user'
        },{
            attributes: ['role_name'],
            model: ApiMarketingRole,
            required: false,
            as: 'apiRole',
        }]
    });

    if(!userData.token && !userData.expires) {
        const error = new Error("Unauthorized.");
        error.statusCode = 401;
        throw error;
    } else {
        const payload = {
            username: user.username,
            role: userData.apiRole.role_name
        }

        const token = jwt.sign(payload, jwtConfig.secret, { expiresIn: jwtConfig.access_token_expires });
        const expiry = jwt.decode(token).exp;

            const rowUpdate = await ApiMarketingUser.update(
                { 'token': token, 'expires': expiry },
                { where: { marketing_user_id: userData.marketing_user_id } }
            );

            if(rowUpdate) {
                const newuserData = await ApiMarketingUser.findOne({
                    where: { 
                        apiRoleId: {
                            [Op.or]: [1, 3]
                        } 
                     }, // 2M
                    include: [{
                        model: User,
                        where: { username: user.username },
                        as: 'user'
                    },{
                        attributes: ['role_name'],
                        model: ApiMarketingRole,
                        required: false,
                        as: 'api_user_role',
                    }]
                });
                req.user = newuserData;
                next();
        }
    } 
}


async function isLogin(req, res, next) {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];

    const { user } = req.user; 
    
    try {
        if(!user) {
            const error = new Error("Unauthorized.");
            error.statusCode = 401;
            throw error;
        }

        const userData = await ApiMarketingUser.findOne({
            where: { 
                apiRoleId: {
                    [Op.or]: [1, 3]
                } 
             }, // 2M
            include: [{
                model: User,
                where: { username: user.username },
                as: 'user'
            },{
                attributes: ['role_name'],
                model: ApiMarketingRole,
                required: false,
                as: 'api_user_role',
            }]
        });

        if(!userData.token && !userData.expires) {
            const error = new Error("Unauthorized.");
            error.statusCode = 401;
            throw error;
        } else {
            if(userData.token != token) {
                const error = new Error("Unauthorized.");
                error.statusCode = 401;
                throw error;
            } else {
                req.user = userData;
                next();
            
            }
           
        }
    } catch(error) {
        next(error);
    }    
}

module.exports = {
    isAuthorized,
    refreshToken,
    isLogin,
    verifyToken
};