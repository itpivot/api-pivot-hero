const express = require('express');
const router = express.Router();
const multer = require('multer');
const path = require('path');
const dipchipController = require('../controllers/dipchipController');
// const authMiddleware = require('../middlewares/authMiddleware');

//http://localhost:3000/report
// router.get('/', reportController.index);
// router.get('/', [ authMiddleware.verifyToken, authMiddleware.isLogin ], reportController.index);

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public/tmp')
    },
    filename: (req, file, cb) => {
        cb(null, 'file-' + new Date().toISOString()+file.originalname)
    }
});

const upload = multer({ 
    storage: storage,
    fileFilter: (req, file, cb) => {
        let allowedExtensions = /(\.jpg|\.jpeg|\.png)$/i;
        if(!allowedExtensions.exec(file.originalname)){
            cb(null, false);
            return cb(new Error('อนุญาติเฉพาะไฟล์ที่เป็นนามสกุล .png .jpg .jpeg เท่านั้น!'));
        } else {
            cb(null, true);
        }
    }
});

router.get('/customers', dipchipController.getIDCardAll);
router.post('/customer/saveidcard', dipchipController.saveIDCard);
router.post('/customer/savephoto', upload.single('photo_image'), dipchipController.savePhoto);
router.post('/customer/workorder', dipchipController.getWorkOrderByBarcode);

// Save Log
router.get('/log', dipchipController.getLog);
router.post('/log/workorder', dipchipController.getLogByBarcode);
router.post('/log/savelog', dipchipController.saveLog);

// Signaturepad
router.post('/customer/savesignature', upload.single('signature_image'), dipchipController.saveSignature);

router.get('/messengers', dipchipController.getIDCardForMessengers);
router.post('/messenger/saveidcard', dipchipController.saveIDCardForMessenger);
router.post('/messenger/savephoto', upload.single('photo_image'), dipchipController.savePhotoForMessenger);

// Return Image
router.get('/messenger/getIDCardImage', dipchipController.getIDCardForMessenger);

module.exports = router;