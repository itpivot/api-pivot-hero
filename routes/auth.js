const express = require('express');
const authController = require('../controllers/authController');
const router = express.Router();

const authMiddleware = require('../middlewares/authMiddleware');

router.get('/', (req, res) => {
    return res.status(200).json({ 'status': 200, 'message': 'Auth Page'});
})

router.post('/login', authController.signIn);
router.post('/logout', authMiddleware.verifyToken, authController.signOut);


module.exports = router;