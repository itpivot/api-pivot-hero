const express = require('express');
const router = express.Router();
const AttendanceController = require('../controllers/attendanceController');

router.get('/',  AttendanceController.getAttendance);
router.post('/checkin', AttendanceController.checkIn);
router.post('/checkout', AttendanceController.checkOut);

module.exports = router;