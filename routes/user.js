const express = require('express');

const db = require('../models');
const User = db.user;
const UserController = require('../controllers/userController');

// test 
const CompanyController = require('../controllers/companyController');

const router = express.Router();

router.get('/profile', UserController.getProfile);

module.exports = router;