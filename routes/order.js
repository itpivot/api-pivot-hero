const express = require('express');
const orderController = require('../controllers/orderController');
const multer = require('multer');
const path = require('path');
const router = express.Router();

const db = require('../models');

// --- รูปภาพเลขไมล์ ----
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        const path = './public/tmp';
        cb(null, path)
    },
    filename: function (req, file, cb) {
        cb(null, new Date().toISOString()+file.originalname)
    },
});

const upload = multer({ 
    storage: storage,
    fileFilter: (req, file, cb) => {
        let allowedExtensions = /(\.jpg|\.jpeg|\.png)$/i;
        if(!allowedExtensions.exec(file.originalname)){
            cb(null, false);
            return cb(new Error('อนุญาติเฉพาะไฟล์ที่เป็นนามสกุล .png .jpg .jpeg เท่านั้น!'));
        } else {
            cb(null, true);
        }
    }
});
// --- รูปภาพเลขไมล์ ----

// --- รูปภาพลายเซ็นต์ ---
const storageSign = multer.memoryStorage();
const uploadSign = multer({ storage: storageSign });
// const uploadSign = multer({ dest: './public/signatures' });
// const uploadSign = multer({ dest: './public/signatures' });
// --- รูปภาพลายเซ็นต์ ---


router.get('/', orderController.getOrders);

router.post('/search', orderController.searchOrders);

router.post('/', orderController.receiveJob);
router.get('/messenger', orderController.getMessengerOrder);

router.post('/messenger/search', orderController.searchMessengerOrders);

router.post('/messenger/return/:order_id', orderController.returnJob);
router.get('/messenger/locations/:order_id', orderController.getLocations);
router.get('/messenger/milenumber', orderController.getLatestMileNumber);
router.post('/messenger/upload', upload.single('mile_image'), orderController.uploadImage);

router.post('/messenger/signature', uploadSign.single('signature'), orderController.uploadSignature);

router.post('/messenger/closejob', orderController.closeJob);

router.get('/messenger/summary', orderController.summaryOrder);
router.post('/messenger/summary/search', orderController.searchsummaryOrder);

router.get('/messenger/location/detail/:location_id', orderController.getDetailOrder);

module.exports = router;