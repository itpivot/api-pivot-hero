const express = require('express');
const router = express.Router();
const reportController = require('../controllers/reportController');
const authMiddleware = require('../middlewares/authMiddleware');

//http://localhost:3000/report
// router.get('/', reportController.index);
router.get('/', [ authMiddleware.verifyToken, authMiddleware.isLogin ], reportController.index);

module.exports = router;