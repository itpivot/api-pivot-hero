const jwtSecret = require('./jwtConfig');
const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;
const { Op } = require("sequelize");

const  EncryptAES = require('../services/encryptServices');

const db = require('../models');
const User = db.user;
const ApiMarketingUser = db.api_marketing_user;
const ApiMarketingRole = db.api_marketing_role;

passport.use(
    'login',
    new localStrategy(
        {
            usernameField: 'username',
            passwordField: 'password',
            session: false
        },
        async (username, password, done) => {

            try {

                const user_api = await ApiMarketingUser.findOne({
                 
                    where: { 
                        apiRoleId: {
                            [Op.or]: [1, 3]
                        }
                    }, // 2M
                    include: [{
                        model: User,
                        where: { username: username },
                        as: 'user'
                    },{
                        attributes: ['role_name'],
                        model: ApiMarketingRole,
                        as: 'api_user_role'
                    }]
                });

                // not exist user
                if(!user_api) {
                    return done(null, false, { message: 'User not found.' });
                } else {
                    // compare password
                    const aesEncrypt = new EncryptAES(password);
                    let isMatch =  aesEncrypt.comparePassword(user_api.user.password);
                  
                    if(isMatch) {
                        if(!isMatch.status) {
                            return done(null, false, { message: 'Wrong Password.' });
                        } 
                        return done(null, user_api.user);
                    }
                }
            } catch(err) {
                done(err);
            }
        }
    )
);


const opts = {
    jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    secretOrKey: jwtSecret.secret
}

passport.use(
    'jwt',
    new JwtStrategy(opts, async (jwt_payload, done) => {
        try {
            const { exp } = jwt_payload;
            if(Date.now() > exp) {
                done(null, false, { message: 'Unauthorized.' });
            }

            const user_api =  await ApiMarketingUser.findOne({
                include: [{
                    model: User,
                    where: { username: jwt_payload.username },
                    as: 'user'
                }]
            });

            if(!user_api.user) {
                console.log('ไม่พบข้อมูล')
                return done(null, false, { message: 'ไม่พบข้อมูล' });
            } else {

                // if(!user_api.token || !apiUser.expires) {
                //     done(null, false, { message: 'Unauthorized.' });
                // }

                console.log('รหัสผ่านถูกต้อง พนักงานเข้าสู่ระบบ')
                return done(null, apiUser.user);
            }
        } catch(err) {
            console.log(err)
            done(err);
        }
    }) 
);

