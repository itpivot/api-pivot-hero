require('dotenv').config();

module.exports = {
    'secret': process.env.JWT_SECRET,
    'refresh_secret': process.env.JWT_REFRESH_SECRET,
    'access_token_expires': process.env.JWT_ACCESS_TOKEN_EXPIRES,
    'refresh_token_expires': process.env.JWT_REFRESH_TOKEN_EXPIRES,
    'access_cookie_expires': process.env.ACCESS_TOKEN_COOKIE_EXPIRES,
    'refresh_cookie_expires': process.env.REFRESH_TOKEN_COOKIE_EXPIRES,
}