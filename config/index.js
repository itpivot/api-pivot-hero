require('dotenv').config();

module.exports = {
    PORT: process.env.PORT,
    DOMAIN: process.env.DOMAIN,
};