insert into api_marketing_company_user (pvs_users_id, pvs_users_role_id, active, username, password, create_date, first_login) values 
(1726, 10, 1, 'PI262', 'pivot123', CURRENT_TIMESTAMP, 1);

insert into api_marketing_company_sender_user (api_marketing_company_id, api_marketing_company_user_id)
select 1, api_marketing_company_user_id
from api_marketing_company_user
where username = 'PI262';

insert into api_marketing_sender (api_marketing_sender_id, name, surname, telephone, department, address, pvs_districts_id, pvs_amphures_id, pvs_provinces_id) values 
(1, 'สหภาพ', 'สีแก้วสิ่ว', '0613918844', 'IT', '1000/151 ลิเบอร์ตี้พลาซ่า', 203, 49, 2),
(2, 'สหภาพ', 'สีแก้วสิ่ว', '0613918844', 'IT', '1000/151 ลิเบอร์ตี้พลาซ่า', 203, 49, 2),
(3, 'สหภาพ', 'สีแก้วสิ่ว', '0613918844', 'IT', '1000/151 ลิเบอร์ตี้พลาซ่า', 203, 49, 2);

insert into api_marketing_receiver (api_marketing_receiver_id, name, surname, telephone, department, address, pvs_districts_id, pvs_amphures_id, pvs_provinces_id) values 
(1, 'สุนทร', 'วงหา', '0613918844', 'บัญชี', '3/1181 ถ.สุขุมวิท 101', 203, 49, 2),
(2, 'อนาพร', 'สุขม่วง', '0613918844', 'IT', '199/249 อาคารไหนก็ได้', 203, 49, 2),
(3, 'เอกพงษ์', 'ชุกสา', '0613918844', 'IT', '100/4', 203, 49, 2);

insert into api_marketing_receiver (api_marketing_receiver_id, name, surname, telephone, department, address, pvs_districts_id, pvs_amphures_id, pvs_provinces_id) values 
(4, 'ภคพร', 'วงษา', '0613918844',  NULL, 'Metris Condominium', 203, 49, 2),
(5, 'อนุพงศ์', 'ไตรรัก', '0613918844', 'IT', 'ตลาดคลองเตย', 203, 49, 2);

insert into api_marketing_receiver (api_marketing_receiver_id, name, surname, telephone, department, address, pvs_districts_id, pvs_amphures_id, pvs_provinces_id) values 
(6, 'ปรีชา', 'ปราชี', '0613918844', 'การตลาด', 'Italthai', 203, 49, 2);


insert into api_marketing_work_order (api_marketing_work_order_id, barcode, date_order, date_meet, api_marketing_company_id, api_marketing_sender_id, api_marketing_status_id, api_marketing_company_user_id, active) values
(1, 'TM202122300001', '2020-12-22', CURRENT_DATE, 1, 1, 1, 1, 1),
(2, 'TM202122300002', '2020-12-22', CURRENT_DATE, 1, 2, 1, 1, 1),
(3, 'TM202122300003', '2020-12-22', CURRENT_DATE, 1, 3, 1, 1, 1);

insert into api_marketing_work_order_location (api_marketing_work_order_location_id, api_marketing_work_order_id, api_marketing_receiver_id, api_marketing_status_id, price, api_type_work_id, detail_type_work, active) values 
(1, 1, 1, 1, 100.00, 1, NULL, 1),
(2, 1, 2, 1, 1500.00, 1, NULL, 1),
(3, 1, 3, 1, 10000.00, 1, NULL, 1);

insert into api_marketing_work_order_location (api_marketing_work_order_location_id, api_marketing_work_order_id, api_marketing_receiver_id, api_marketing_status_id, price, api_type_work_id, detail_type_work, active) values 
(4, 2, 4, 1, 25000.00, 1, NULL, 1),
(5, 2, 5, 1, 300000.00, 1, NULL, 1);

insert into api_marketing_work_order_location (api_marketing_work_order_location_id, api_marketing_work_order_id, api_marketing_receiver_id, api_marketing_status_id, price, api_type_work_id, detail_type_work, active) values 
(6, 3, 6, 1, 1000000.00, 1, NULL, 1);
