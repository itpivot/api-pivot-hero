
INSERT INTO pvs_messenger_team (pvs_messenger_team_id, team_name, pvs_branch_id, create_date, create_at) VALUES
(18, '2M', 1, CURRENT_TIMESTAMP, 103),
(19, 'MM', 1, CURRENT_TIMESTAMP, 103);

INSERT INTO pvs_messenger_team_leader (pvs_messenger_team_id, pvs_users_id, create_date, create_at) VALUES 
(18, 1346, CURRENT_TIMESTAMP, 103);


-- Role MM / 2M จะเก็บใน api_marketing_user
CREATE TABLE api_marketing_role (
    api_marketing_role_id   INT AUTO_INCREMENT,
    api_marketing_role_name VARCHAR(150),
    CONSTRAINT p_api_marketing_role PRIMARY KEY (api_marketing_role_id)
) ENGINE = INNODB;

INSERT INTO api_marketing_role (api_marketing_role_name) VALUES 
('2M'),
('MM'),
('Rescue');

-- User เฉพาะแมสที่เป็น 2M/MM เท่านั้น
CREATE TABLE api_marketing_user (
    api_marketing_user_id       BIGINT AUTO_INCREMENT,
    system_timestamp            TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
    pvs_users_id                INT DEFAULT NULL,
    token                       VARCHAR(300),
    expires                      VARCHAR(150),
    api_marketing_role_id       INT DEFAULT NULL,
    active                      TINYINT DEFAULT 1,
    CONSTRAINT p_api_marketing_user PRIMARY KEY (api_marketing_user_id),
    CONSTRAINT f_api_marketing_user_1 FOREIGN KEY (pvs_users_id) REFERENCES pvs_users(pvs_users_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_user_2 FOREIGN KEY (api_marketing_role_id) REFERENCES api_marketing_role(api_marketing_role_id) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE = INNODB;

-- ประเภท Check-in / Check-out
CREATE TABLE api_marketing_checkin_type (
    api_marketing_checkin_type_id INT AUTO_INCREMENT,
    api_marketing_checkin_type_name VARCHAR(50),
    CONSTRAINT p_api_marketing_checkin_type PRIMARY KEY (api_marketing_checkin_type_id)
) ENGINE = INNODB;

INSERT INTO api_marketing_checkin_type (api_marketing_checkin_type_id, api_marketing_checkin_type_name) VALUES 
(1, 'CHECK IN'),
(2, 'CHECK OUT');


-- เก็บ Log Check in / Check out
CREATE TABLE api_marketing_checkin (
    api_marketing_checkin_id        BIGINT AUTO_INCREMENT,
    system_timestamp                TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
    api_marketing_user_id           BIGINT DEFAULT NULL,
    api_marketing_checkin_type_id   INT,                
    latitude                        VARCHAR(150),
    longtitude                      VARCHAR(150),
    CONSTRAINT p_api_marketing_checkin PRIMARY KEY (api_marketing_checkin_id),
    CONSTRAINT f_api_marketing_checkin_1 FOREIGN KEY (api_marketing_checkin_type_id) REFERENCES api_marketing_checkin_type(api_marketing_checkin_type_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_checkin_2 FOREIGN KEY (api_marketing_user_id) REFERENCES api_marketing_user(api_marketing_user_id) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE = INNODB;


-- สถานะการอนุมัติ/ไม่อนุมัติ
CREATE TABLE api_mm_status (
    api_mm_status_id                INT AUTO_INCREMENT,
    system_timestamp                TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
    status_name                     VARCHAR(150),
    CONSTRAINT p_api_mm_status PRIMARY KEY (api_mm_status_id)
) ENGINE = INNODB;

INSERT INTO api_mm_status (api_mm_status_id, status_name) VALUES 
(1, 'รอการตรวจสอบ'),
(2, 'อนุมัติ'),
(3, 'ไม่อนุมัติ'),
(4, 'ยกเลิก');

-- สถานะการจ่ายเงิน
CREATE TABLE api_paid_status (
    api_paid_status_id                INT AUTO_INCREMENT,
    system_timestamp                TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
    status_name                     VARCHAR(150),
    CONSTRAINT p_api_paid_status PRIMARY KEY (api_paid_status_id)
) ENGINE = INNODB;

INSERT INTO api_paid_status (api_paid_status_id, status_name) VALUES 
(1, 'รอจ่ายค่าแนะนำ'),
(2, 'ตรวจสอบค่าแนะนำแล้ว'),
(3, 'จ่ายค่าแนะนำแล้ว'),
(4, 'รอจ่ายค่า Commission'),
(5, 'ตรวจสอบค่า Commission แล้ว'),
(6, 'จ่ายค่า Commission แล้ว'),
(7, 'ยกเลิกจ่ายค่าแนะนำแล้ว'),
(8, 'ยกเลิกจ่ายค่า Commission แล้ว');

-- ค่าใช้จ่าย
CREATE TABLE api_marketing_cost (
    api_marketing_cost_id INT AUTO_INCREMENT,
    system_timestamp      TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
    cost_name             VARCHAR(150),
    cost_price            DECIMAL(19,2), 
    CONSTRAINT p_api_marketing_cost PRIMARY KEY (api_marketing_cost_id)
)  ENGINE = INNODB;

INSERT INTO api_marketing_cost (cost_name, cost_price) VALUES
('ยูนิฟอร์ม 1000 บาทต่อปี', 83.00),
('สวัสดิการ สปส 7% ต่อเดือน', 840.00),
('ประกัน 2500 บาทต่อปี', 208.00),
('Severance Pay 1 เดือนต่อปี', 1000.00),
('เงินสบทบประจำปี', 83.00),
('ตรวจสุขภาพ/ประวัติอาชญากรรม', 83.00), 
('ค่านายหน้า', 800.00),
('หัวหน้างาน', 30000.00), 
('HR', 15000.00), 
('Finance', 15000.00), 
('Admin', 15000.00);

CREATE TABLE api_marketing_service (
    api_marketing_service_id INT AUTO_INCREMENT,
    service_name             VARCHAR(200),
    CONSTRAINT p_api_marketing_service PRIMARY KEY (api_marketing_service_id)
) ENGINE = INNODB;

INSERT INTO api_marketing_service (api_marketing_service_id, service_name) VALUES 
(1, 'แมสเซ็นเจอร์รายเดือน'),
(2, 'อื่นๆ');

-- ตารางเก็บรายชื่อบริษัท ที่ MM แนะนำมาได้
CREATE TABLE api_marketing_company (
    api_marketing_company_id            BIGINT AUTO_INCREMENT,
    system_timestamp                    TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
    company_code                        VARCHAR(200),
    company_name                        VARCHAR(300),
    contact_name                        VARCHAR(300),
    telephone_1                         VARCHAR(150),
    telephone_2                         VARCHAR(150),
    email                               VARCHAR(150),
    address                             VARCHAR(200),
    pvs_districts_id                    INT,
    pvs_amphures_id                     INT,
    pvs_provinces_id                    INT,
    tax_number                          VARCHAR(200) DEFAULT NULL,
    latitude                            VARCHAR(200),
    longtitude                          VARCHAR(200),
    api_marketing_user_id               BIGINT,
    api_mm_status_id                    INT,
    api_paid_status_id                  INT,
    active                              TINYINT DEFAULT 1,
    billing_date                        DATE DEFAULT NULL,
    date_approve_incentive              DATE DEFAULT NULL,
    date_approve_commission             DATE DEFAULT NULL,
    date_paid_incentive                 DATE DEFAULT NULL,
    date_paid_commission                DATE DEFAULT NULL,
    api_marketing_service_id            INT,
    other_service                       VARCHAR(300) DEFAULT NULL,
    remark                              TEXT,
    remark_hr                           TEXT,
    CONSTRAINT p_api_marketing_company PRIMARY KEY (api_marketing_company_id),
    CONSTRAINT f_api_marketing_company_1 FOREIGN KEY (pvs_districts_id) REFERENCES pvs_districts (pvs_districts_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_company_2 FOREIGN KEY (pvs_amphures_id) REFERENCES pvs_amphures (pvs_amphures_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_company_3 FOREIGN KEY (pvs_provinces_id) REFERENCES pvs_provinces (pvs_provinces_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_company_4 FOREIGN KEY (api_marketing_user_id) REFERENCES api_marketing_user (api_marketing_user_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_company_5 FOREIGN KEY (api_mm_status_id) REFERENCES api_mm_status (api_mm_status_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_company_6 FOREIGN KEY (api_paid_status_id) REFERENCES api_paid_status (api_paid_status_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_company_7 FOREIGN KEY (api_marketing_service_id) REFERENCES api_marketing_service (api_marketing_service_id) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE = INNODB;

-- เก็บวันวางบิล
CREATE TABLE api_marketing_billing_date (
    api_marketing_billing_date_id       BIGINT AUTO_INCREMENT,
    system_timestamp                    TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
    billing_date                        DATE,
    api_marketing_company_id            BIGINT,
    pvs_users_id                        INT,
    CONSTRAINT p_api_marketing_billing_date PRIMARY KEY (api_marketing_billing_date_id),
    CONSTRAINT f_api_marketing_billing_date_1 FOREIGN KEY (api_marketing_company_id) REFERENCES api_marketing_company (api_marketing_company_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_billing_date_2 FOREIGN KEY (pvs_users_id) REFERENCES pvs_users (pvs_users_id) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE = INNODB;

-- เก็บค่าใช้จ่าย ที่ Marketing กรอกเมื่ออนุมัติ
CREATE TABLE api_marketing_company_cost (
    api_marketing_company_cost_id   BIGINT AUTO_INCREMENT,
    system_timestamp                TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
    api_marketing_company_id        BIGINT,
    sale_price                      DECIMAL(19,2),
    salary                          DECIMAL(19,2),
    gasoline                        DECIMAL(19,2),
    diligence                       DECIMAL(19,2),  
    overtime_pay                    DECIMAL(19,2),  
    distance_exceed_pay             DECIMAL(19,2),  
    uniform                         DECIMAL(19,2),
    social_security_tax             DECIMAL(19,2),
    insurance                       DECIMAL(19,2),
    severance_pay                   DECIMAL(19,2),
    employee_contribution           DECIMAL(19,2),
    personal_checkup                DECIMAL(19,2),
    incentive                       DECIMAL(19,2), 
    incentive_avg                   DECIMAL(19,2), 
    commission                      DECIMAL(19,2),
    supervisor_salary               DECIMAL(19,2),
    hr_cost                         DECIMAL(19,2),
    hr_avg                          DECIMAL(19,2),
    finance_cost                    DECIMAL(19,2),
    administrator_cost              DECIMAL(19,2),
    backup_cost                     DECIMAL(19,2),
    messengers                      INT,
    minimum_kilometer               INT,
    contract_start_date             DATE,
    contract_end_date               DATE,
    start_work_time                 TIME,
    end_work_time                   TIME,
    start_work_date                 DATE,
    pvs_users_id                    INT,
    CONSTRAINT p_api_marketing_company_cost PRIMARY KEY (api_marketing_company_cost_id),
    CONSTRAINT f_api_marketing_company_cost_1 FOREIGN KEY (api_marketing_company_id) REFERENCES api_marketing_company (api_marketing_company_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_company_cost_2 FOREIGN KEY (pvs_users_id) REFERENCES pvs_users (pvs_users_id) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE = INNODB;

-- เก็บ Log การเปลี่ยนสถานะการจ่ายตังค์
CREATE TABLE api_marketing_paid_activity (
    api_marketing_paid_activity_id BIGINT AUTO_INCREMENT,
    system_timestamp               TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
    api_marketing_company_id       BIGINT,
    api_paid_status_id             INT,
    pvs_users_id                   INT, 
    CONSTRAINT p_api_marketing_paid_activity PRIMARY KEY (api_marketing_paid_activity_id),
    CONSTRAINT f_api_marketing_paid_activity_1 FOREIGN KEY (api_paid_status_id) REFERENCES api_paid_status (api_paid_status_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_paid_activity_2 FOREIGN KEY (api_marketing_company_id) REFERENCES api_marketing_company (api_marketing_company_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_paid_activity_3 FOREIGN KEY (pvs_users_id) REFERENCES pvs_users (pvs_users_id) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE = INNODB;

-- User ของลูกค้า
CREATE TABLE api_marketing_company_user (
    api_marketing_company_user_id   BIGINT AUTO_INCREMENT,
    system_timestamp                TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
    pvs_users_id                    INT,
    api_marketing_company_id        BIGINT,
    token                           VARCHAR(300) DEFAULT NULL,
    expires                         VARCHAR(150) DEFAULT NULL,
    department_name                 VARCHAR(250) DEFAULT NULL,
    active                          TINYINT DEFAULT 1,
    CONSTRAINT p_api_marketing_company_user PRIMARY KEY (api_marketing_company_user_id),
    CONSTRAINT f_api_marketing_company_user_1 FOREIGN KEY (pvs_users_id) REFERENCES pvs_users (pvs_users_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_company_user_2 FOREIGN KEY (api_marketing_company_id) REFERENCES api_marketing_company (api_marketing_company_id) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE = INNODB;

-- Log การเข้าใช้งานของ User ลูกค้า
CREATE TABLE api_marketing_company_user_log (
    api_marketing_company_user_log_id   BIGINT AUTO_INCREMENT,
    system_timestamp                    TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
    api_marketing_company_user_id       BIGINT,
    activity                            VARCHAR(150),
    status                              VARCHAR(150),
    ip_address                          VARCHAR(150),
    description                         VARCHAR(150),
    CONSTRAINT p_api_marketing_company_user_log PRIMARY KEY (api_marketing_company_user_log_id),
    CONSTRAINT f_api_marketing_company_user_log_1 FOREIGN KEY (api_marketing_company_user_id) REFERENCES api_marketing_company_user (api_marketing_company_user_id) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE = INNODB;

-- ตารางเก็บ แมสเซ็นเจอร์ 2m ที่ผูกกับ บริษัท
CREATE TABLE api_marketing_company_messenger_user ( 
    api_marketing_company_messenger_user_id BIGINT AUTO_INCREMENT,
    system_timestamp                        TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
    api_marketing_company_id                BIGINT,
    api_marketing_user_id                   BIGINT,
    CONSTRAINT p_api_marketing_company_messenger_user PRIMARY KEY (api_marketing_company_messenger_user_id),
    CONSTRAINT f_api_marketing_company_messenger_user_1 FOREIGN KEY (api_marketing_company_id) REFERENCES api_marketing_company (api_marketing_company_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_company_messenger_user_2 FOREIGN KEY (api_marketing_user_id) REFERENCES api_marketing_user (api_marketing_user_id) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE = INNODB;

-- ตารางเก็บ แมสเซ็นเจอร์ ที่มาปฏิบัติงาน (เท่านั้น) 
-- active 0 คือ ยังไม่ check-in / 1 = คือ check-in แล้ว เมื่อ Add มาจะเริ่มต้น 0 เสมอจนกว่าจะ  check-in 
CREATE TABLE api_marketing_company_messenger_active ( 
    api_marketing_company_messenger_active_id BIGINT AUTO_INCREMENT,
    system_timestamp                        TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
    started_at                              DATETIME DEFAULT NULL,
    api_marketing_company_id                BIGINT,
    api_marketing_user_id                   BIGINT,
    active                                  TINYINT DEFAULT 0,
    CONSTRAINT p_api_marketing_company_messenger_active PRIMARY KEY (api_marketing_company_messenger_active_id),
    CONSTRAINT f_api_marketing_company_messenger_active_1 FOREIGN KEY (api_marketing_company_id) REFERENCES api_marketing_company (api_marketing_company_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_company_messenger_active_2 FOREIGN KEY (api_marketing_user_id) REFERENCES api_marketing_user (api_marketing_user_id) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE = INNODB;



-- ตารางเก็บข้อมูลผู้ส่ง (เผื่อทำ auto-complete)
CREATE TABLE api_marketing_company_sender_user ( 
    api_marketing_company_sender_user_id BIGINT AUTO_INCREMENT,
    system_timestamp                    TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
    api_marketing_company_id            BIGINT,
    api_marketing_company_user_id       BIGINT,
    CONSTRAINT p_api_marketing_company_sender_user PRIMARY KEY (api_marketing_company_sender_user_id),
    CONSTRAINT f_api_marketing_company_sender_user_1 FOREIGN KEY (api_marketing_company_id) REFERENCES api_marketing_company (api_marketing_company_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_company_sender_user_2 FOREIGN KEY (api_marketing_company_user_id) REFERENCES api_marketing_company_user (api_marketing_company_user_id) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE = INNODB;

-- สถานะงานของ 2M
CREATE TABLE api_marketing_status (
    api_marketing_status_id         INT AUTO_INCREMENT,
    system_timestamp                TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
    status_name                     VARCHAR(150),
    CONSTRAINT p_api_marketing_status PRIMARY KEY (api_marketing_status_id)
) ENGINE = INNODB;

INSERT INTO api_marketing_status (api_marketing_status_id, status_name) VALUES
(1, 'งานใหม่'),
(2, 'กำลังดำเนินการ'),
(3, 'สำเร็จ'),
(4, 'ไม่สำเร็จ'),
(5, 'งานตาม'),
(6, 'งานยกเลิก');




-- ตารางเก็บข้อมูลผู้ส่งต่องานเมื่อลูกค้าสั่งงาน 2M
CREATE TABLE api_marketing_sender (
    api_marketing_sender_id             BIGINT AUTO_INCREMENT,
    system_timestamp                    TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
    name                                VARCHAR(150),
    surname                             VARCHAR(150),
    telephone                           VARCHAR(150),
    department                           VARCHAR(150),
    address                             VARCHAR(150),
    pvs_districts_id                    INT,
    pvs_amphures_id                     INT,
    pvs_provinces_id                    INT,
    CONSTRAINT p_api_marketing_sender PRIMARY KEY (api_marketing_sender_id),
    CONSTRAINT f_api_marketing_sender_1 FOREIGN KEY (pvs_districts_id) REFERENCES pvs_districts (pvs_districts_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_sender_2 FOREIGN KEY (pvs_amphures_id) REFERENCES pvs_amphures (pvs_amphures_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_sender_3 FOREIGN KEY (pvs_provinces_id) REFERENCES pvs_provinces (pvs_provinces_id) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE = INNODB;


-- ตารางเก็บข้อมูลผู้รับต่องานเมื่อลูกค้าสั่งงาน 2M
CREATE TABLE api_marketing_receiver (
    api_marketing_receiver_id             BIGINT AUTO_INCREMENT,
    system_timestamp                    TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
    name                                VARCHAR(150),
    surname                             VARCHAR(150),
    telephone                           VARCHAR(150),
    department                           VARCHAR(150),
    address                             VARCHAR(150),
    pvs_districts_id                    INT,
    pvs_amphures_id                     INT,
    pvs_provinces_id                    INT,
    zipcode                             VARCHAR(150),
    CONSTRAINT p_api_marketing_receiver PRIMARY KEY (api_marketing_receiver_id),
    CONSTRAINT f_api_marketing_receiver_1 FOREIGN KEY (pvs_districts_id) REFERENCES pvs_districts (pvs_districts_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_receiver_2 FOREIGN KEY (pvs_amphures_id) REFERENCES pvs_amphures (pvs_amphures_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_receiver_3 FOREIGN KEY (pvs_provinces_id) REFERENCES pvs_provinces (pvs_provinces_id) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE = INNODB;


-- ประเภทงาน 2M
CREATE TABLE api_type_work (
    api_type_work_id                INT AUTO_INCREMENT,
    system_timestamp                TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
    type_name                       VARCHAR(150),
    CONSTRAINT p_api_type_work PRIMARY KEY (api_type_work_id)
) ENGINE = INNODB;

INSERT INTO api_type_work (api_type_work_id, type_name) VALUES 
(1, 'เก็บเช็ค - วางบิล'),
(2, 'รับเอกสาร'),
(3, 'ส่งเอกสาร'),
(4, 'จ่ายบิล'),
(5, 'พัสดุ'),
(6, 'อื่นๆ');

-- เก็บข้อมูลการสั่งของลูกค้า 2M
CREATE TABLE api_marketing_work_order (
    api_marketing_work_order_id         BIGINT AUTO_INCREMENT,
    system_timestamp                    TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
    barcode                             VARCHAR(200) DEFAULT NULL,
    date_order                          DATE,
    date_meet                           DATE,
    date_closejob                       DATETIME,
    date_receive                        DATETIME,
    api_marketing_company_id            BIGINT,
    api_marketing_sender_id             BIGINT,
    api_marketing_status_id             INT,
    api_marketing_company_user_id       BIGINT,
    api_marketing_user_id               BIGINT,
    active                              TINYINT DEFAULT 1,
    CONSTRAINT p_api_marketing_work_order PRIMARY KEY (api_marketing_work_order_id),
    CONSTRAINT f_api_marketing_work_order_1 FOREIGN KEY (api_marketing_company_id) REFERENCES api_marketing_company (api_marketing_company_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_work_order_2 FOREIGN KEY (api_marketing_sender_id) REFERENCES api_marketing_sender (api_marketing_sender_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_work_order_4 FOREIGN KEY (api_marketing_company_user_id) REFERENCES api_marketing_company_user (api_marketing_company_user_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_work_order_5 FOREIGN KEY (api_marketing_status_id) REFERENCES api_marketing_status (api_marketing_status_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_work_order_7 FOREIGN KEY (api_marketing_user_id) REFERENCES api_marketing_user (api_marketing_user_id) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE = INNODB;


CREATE TABLE api_marketing_work_order_location (
    api_marketing_work_order_location_id BIGINT AUTO_INCREMENT,
    system_timestamp                    TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
    date_closejob                       DATETIME,
    barcode                             VARCHAR(200) DEFAULT NULL,
    api_marketing_work_order_id         BIGINT,
    api_marketing_receiver_id           BIGINT,
    api_marketing_status_id             INT,
    price                               DECIMAL(19,2) DEFAULT 0, 
    api_type_work_id                    INT,
    detail_type_work                    VARCHAR(200),
    order_remark                        TEXT,
    messenger_remark                    TEXT,
    active                              TINYINT DEFAULT 1,
    api_marketing_user_id               BIGINT,
    CONSTRAINT p_api_marketing_work_order_location PRIMARY KEY (api_marketing_work_order_location_id),
    CONSTRAINT f_api_marketing_work_order_location_1 FOREIGN KEY (api_marketing_work_order_id) REFERENCES api_marketing_work_order(api_marketing_work_order_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_work_order_location_2 FOREIGN KEY (api_marketing_receiver_id) REFERENCES api_marketing_receiver (api_marketing_receiver_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_work_order_location_3 FOREIGN KEY (api_marketing_status_id) REFERENCES api_marketing_status (api_marketing_status_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_work_order_location_4 FOREIGN KEY (api_type_work_id) REFERENCES api_type_work (api_type_work_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_work_order_location_5 FOREIGN KEY (api_marketing_user_id) REFERENCES api_marketing_user (api_marketing_user_id) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE = INNODB;

-- เก็บเลข Miles
CREATE TABLE api_marketing_work_order_mile (
    api_marketing_work_order_mile_id            BIGINT AUTO_INCREMENT,
    system_timestamp                            TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
    api_marketing_work_order_location_id        BIGINT,
    mile_number                                 VARCHAR(150),
    picture                                     VARCHAR(200),
    latitude                                    VARCHAR(150),
    longtitude                                  VARCHAR(150),
    active                                      TINYINT DEFAULT 1,
    location_type                               VARCHAR(50) DEFAULT NULL,
    api_marketing_user_id                       BIGINT,
    CONSTRAINT p_api_marketing_work_order_mile PRIMARY KEY (api_marketing_work_order_mile_id),
    CONSTRAINT f_api_marketing_work_order_mile_1 FOREIGN KEY (api_marketing_work_order_location_id) REFERENCES api_marketing_work_order_location (api_marketing_work_order_location_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_work_order_mile_2 FOREIGN KEY (api_marketing_user_id) REFERENCES api_marketing_user (api_marketing_user_id) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE = INNODB;

-- ตารางเก็บข้อมูลรับ (เผื่อทำ auto-complete)
CREATE TABLE api_marketing_company_receiver (
    api_marketing_company_receiver_id   BIGINT AUTO_INCREMENT,
    system_timestamp                    TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
    api_marketing_company_user_id       BIGINT,
    name                                VARCHAR(150),
    surname                             VARCHAR(150),
    department                           VARCHAR(150) DEFAULT NULL,
    telephone                           VARCHAR(150) DEFAULT NULL,
    address                             VARCHAR(150),
    pvs_districts_id                    INT,
    pvs_amphures_id                     INT,
    pvs_provinces_id                    INT,
    active                              TINYINT DEFAULT 1,
    CONSTRAINT p_api_marketing_company_receiver PRIMARY KEY (api_marketing_company_receiver_id),
    CONSTRAINT f_api_marketing_company_receiver_1 FOREIGN KEY (api_marketing_company_user_id) REFERENCES api_marketing_company_user (api_marketing_company_user_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_company_receiver_2 FOREIGN KEY (pvs_districts_id) REFERENCES pvs_districts (pvs_districts_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_company_receiver_3 FOREIGN KEY (pvs_amphures_id) REFERENCES pvs_amphures (pvs_amphures_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_company_receiver_4 FOREIGN KEY (pvs_provinces_id) REFERENCES pvs_provinces (pvs_provinces_id) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE = INNODB; 

-- ตารางเก็บส่วนต่าง Miles และ OT
CREATE TABLE api_marketing_over_performance (
    api_marketing_over_performance_id   BIGINT AUTO_INCREMENT,
    system_timestamp                    TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
    api_marketing_user_id               BIGINT,
    overtime                            DECIMAL(19,2),
    distance_exceed                     DECIMAL(19,2),
    CONSTRAINT api_marketing_over_performance PRIMARY KEY (api_marketing_over_performance_id),
    CONSTRAINT api_marketing_over_performance_1 FOREIGN KEY (api_marketing_user_id) REFERENCES api_marketing_user (api_marketing_user_id) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE = INNODB; 


CREATE TABLE api_marketing_work_order_signature (
    api_marketing_work_order_signature_id            BIGINT AUTO_INCREMENT,
    system_timestamp                            TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
    api_marketing_work_order_location_id        BIGINT,
    picture                                     VARCHAR(200),
    api_marketing_user_id                       BIGINT,
    CONSTRAINT p_api_marketing_work_order_signature PRIMARY KEY (api_marketing_work_order_signature_id),
    CONSTRAINT f_api_marketing_work_order_signature_1 FOREIGN KEY (api_marketing_work_order_location_id) REFERENCES api_marketing_work_order_location (api_marketing_work_order_location_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_marketing_work_order_signature_2 FOREIGN KEY (api_marketing_user_id) REFERENCES api_marketing_user (api_marketing_user_id) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE = INNODB;


