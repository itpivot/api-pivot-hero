DROP TABLE IF EXISTS api_dipchip_card_reader;
CREATE TABLE api_dipchip_card_reader (
    api_dipchip_card_reader_id  INT AUTO_INCREMENT,
    system_timestamp      TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
    work_order_barcode    VARCHAR(100),
    work_order_id         INT(11),
    customer_id           INT(11),
    card_id               VARBINARY(500),
    name_title            VARBINARY(500),
    name                  VARBINARY(500),
    name_mid              VARBINARY(500),
    surname               VARBINARY(500),
    name_title_en         VARBINARY(500),
    name_en               VARBINARY(500),
    name_mid_en           VARBINARY(500),
    surname_en            VARBINARY(500),
    address_no            VARCHAR(100),
    address_moo           VARCHAR(100),
    address_sub_alley     VARCHAR(100),
    address_alley         VARCHAR(100),
    road                  VARCHAR(100),
    sub_district          VARCHAR(100),
    district              VARCHAR(100),
    province              VARCHAR(100),
    sex                   VARCHAR(5),
    date_of_birth         VARCHAR(8),
    location_of_birth     VARCHAR(100),
    date_of_issue         VARCHAR(8),
    date_of_expiry        VARCHAR(8),
    id_of_picture         VARCHAR(20),
    remark                VARCHAR(500),
    picture               VARCHAR(200),
    CONSTRAINT p_api_dipchip_card_reader PRIMARY KEY (api_dipchip_card_reader_id),
    CONSTRAINT f_api_dipchip_card_reader_1 FOREIGN KEY (work_order_id) REFERENCES pvs_work_order (pvs_work_order_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_dipchip_card_reader_2 FOREIGN KEY (customer_id) REFERENCES pvs_customer (pvs_customer_id) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE = INNODB;

DROP TABLE IF EXISTS api_dipchip_messenger;
CREATE TABLE api_dipchip_messenger (
    api_dipchip_messenger_id  INT AUTO_INCREMENT,
    system_timestamp      TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
    card_id               VARBINARY(500),
    name_title            VARBINARY(500),
    name                  VARBINARY(500),
    name_mid              VARBINARY(500),
    surname               VARBINARY(500),
    name_title_en         VARBINARY(500),
    name_en               VARBINARY(500),
    name_mid_en           VARBINARY(500),
    surname_en            VARBINARY(500),
    address_no            VARCHAR(100),
    address_moo           VARCHAR(100),
    address_sub_alley     VARCHAR(100),
    address_alley         VARCHAR(100),
    road                  VARCHAR(100),
    sub_district          VARCHAR(100),
    district              VARCHAR(100),
    province              VARCHAR(100),
    sex                   VARCHAR(5),
    date_of_birth         VARCHAR(8),
    location_of_birth     VARCHAR(100),
    date_of_issue         VARCHAR(8),
    date_of_expiry        VARCHAR(8),
    id_of_picture         VARCHAR(20),
    remark                VARCHAR(500),
    picture               VARCHAR(200),
    CONSTRAINT p_api_dipchip_messenger PRIMARY KEY (api_dipchip_messenger_id)
) ENGINE = INNODB;

-- เพิ่ม API สำหรับ ตรวจสอบการทำ E-KYC ผลลัพธ์ 
-- status_id 1 = Successs , 2 Unsuccess 
-- Log Description เชื่อมต่ออุปกรณ์ไม่ได้ , อ่านบัตรประชาชนไม่ได้ , ลูกค้าไม่ยินยอม

DROP TABLE IF EXISTS api_dipchip_card_reader_log;
CREATE TABLE api_dipchip_card_reader_log (
    api_dipchip_card_reader_log_id  INT AUTO_INCREMENT,
    system_timestamp      TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
    work_order_barcode    VARCHAR(100),
    work_order_id         INT(11),
    customer_id           INT(11),
    status_id             INT(1),
    log_description       VARCHAR(100),
    CONSTRAINT p_api_dipchip_card_reader_log_id PRIMARY KEY (api_dipchip_card_reader_log_id),
    CONSTRAINT f_api_dipchip_card_reader_log_id_1 FOREIGN KEY (work_order_id) REFERENCES pvs_work_order (pvs_work_order_id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT f_api_dipchip_card_reader_log_id_2 FOREIGN KEY (customer_id) REFERENCES pvs_customer (pvs_customer_id) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE = INNODB;