require('dotenv').config();
const Sequelize = require('sequelize');
// const env = process.env.NODE_ENV || 'development';
const env = process.env.NODE_ENV;
const dbConfig = require('../config/db.config.js')[env];

const EmployeeModel                         = require('../models/employee');
const UserModel                             = require('../models/user');
const ApiMarketingUserModel                 = require('../models/api_marketing_user');
const ApiMarketingRoleModel                 = require('../models/api_marketing_role');
const ApiMarketingCheckinModel              = require('../models/api_marketing_checkin');
const ApiMarketingCheckinTypeModel          = require('../models/api_marketing_checkin_type');
const ApiMarketingWorkOrderModel            = require('../models/api_marketing_work_order');
const ApiMarketingWorkOrderLocationModel    = require('../models/api_marketing_work_order_location');
const ApiMarketingWorkOrderMileModel        = require('../models/api_marketing_work_order_mile');
const ApiMarketingWorkOrderSignatureModel   = require('../models/api_marketing_work_order_signature');
const ApiMarketingCompanyModel              = require('../models/api_marketing_company');
const ApiMarketingStatusModel               = require('../models/api_marketing_status');
const ApiMarketingCompanyMessengerUserModel = require('../models/api_marketing_company_messenger_user');
const ApiMarketingCompanyMessengerActiveModel = require('../models/api_marketing_company_messenger_active');
const ApiMarketingSenderModel               = require('../models/api_marketing_sender');
const ApiMarketingReceiverModel             = require('../models/api_marketing_receiver');
const ApiTypeWorkModel                      = require('../models/api_type_work');

const DistrictModel                         = require('../models/district');
const AmphureModel                         = require('../models/amphure');
const ProvinceModel                         = require('../models/province');

// สำหรับ Project Dip Chip บัตรประชาชน Demo
const DipchipCardReaderModel                = require('../models/api_dipchip_card_reader');
const DipchipMessengerModel                = require('../models/api_dipchip_messenger');
const PvsWorkOrderModel                     = require('../models/pvs_work_order');
const PvsCustomerModel                      = require('../models/pvs_customer');
const DipchipCardReaderLogModel             = require('../models/api_dipchip_card_reader_log');

const sequelize = new Sequelize(dbConfig.database, dbConfig.username, dbConfig.password, {
    host: dbConfig.host,
    dialect: dbConfig.dialect,
    // comment on production
    dialectOptions: {
      // socketPath: '/Applications/MAMP/tmp/mysql/mysql.sock',
      supportBigNumbers: true,
      bigNumberStrings: true,
      timezone: "local",
    },
    timezone: 'Asia/Bangkok',
    port: dbConfig.port,
    pool: {
        max: Number(dbConfig.pool.max),
        min: Number(dbConfig.pool.min),
        acquire: Number(dbConfig.pool.acquire),
        idle: Number(dbConfig.pool.idle)
    },
    // logging: (dbConfig.logging == "true" ? true : false) // disabled Excute default
    logging: false // disabled Excute default
});

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.employee                             = EmployeeModel(sequelize, Sequelize);
db.user                                 = UserModel(sequelize, Sequelize);
db.api_marketing_user                   = ApiMarketingUserModel(sequelize, Sequelize);
db.api_marketing_checkin                = ApiMarketingCheckinModel(sequelize, Sequelize);
db.api_marketing_checkin_type           = ApiMarketingCheckinTypeModel(sequelize, Sequelize);
db.api_marketing_role                   = ApiMarketingRoleModel(sequelize, Sequelize);
db.api_marketing_work_order             = ApiMarketingWorkOrderModel(sequelize, Sequelize);
db.api_marketing_work_order_location    = ApiMarketingWorkOrderLocationModel(sequelize, Sequelize);
db.api_marketing_work_order_mile        = ApiMarketingWorkOrderMileModel(sequelize, Sequelize);
db.api_marketing_work_order_signature   = ApiMarketingWorkOrderSignatureModel(sequelize, Sequelize);
db.api_marketing_company                = ApiMarketingCompanyModel(sequelize, Sequelize);
db.api_marketing_status                 = ApiMarketingStatusModel(sequelize, Sequelize);
db.api_marketing_company_messenger_user = ApiMarketingCompanyMessengerUserModel(sequelize, Sequelize);
db.api_marketing_company_messenger_active = ApiMarketingCompanyMessengerActiveModel(sequelize, Sequelize);
db.api_marketing_sender                 = ApiMarketingSenderModel(sequelize, Sequelize);
db.api_marketing_receiver               = ApiMarketingReceiverModel(sequelize, Sequelize);
db.api_type_work                        = ApiTypeWorkModel(sequelize, Sequelize);
db.district                             = DistrictModel(sequelize, Sequelize);
db.amphure                              = AmphureModel(sequelize, Sequelize);
db.province                             = ProvinceModel(sequelize, Sequelize);
// สำหรับ Project Dip Chip บัตรประชาชน Demo
db.api_dipchip_card_reader              = DipchipCardReaderModel(sequelize, Sequelize);
db.api_dipchip_messenger                = DipchipMessengerModel(sequelize, Sequelize);
db.pvs_work_order                       = PvsWorkOrderModel(sequelize, Sequelize);
db.pvs_customer                         = PvsCustomerModel(sequelize, Sequelize);
db.api_dipchip_card_reader_log          = DipchipCardReaderLogModel(sequelize, Sequelize);

db.user.belongsTo(db.employee, { foreignKey: 'employeeId',  targetKey: 'employee_id', as: 'employee' });
db.employee.hasOne(db.user, { foreignKey: 'employeeId',  targetKey: 'employee_id', as: 'user' });

db.api_marketing_user.belongsTo(db.user, { foreignKey: 'userId', targetKey: 'user_id', as: 'user' });
db.user.hasOne(db.api_marketing_user, { foreignKey: 'userId',  targetKey: 'user_id', as: 'api_user' });

db.api_marketing_user.belongsTo(db.api_marketing_role, { foreignKey: 'apiRoleId', targetKey: 'marketing_role_id', as: 'api_user_role' });
db.api_marketing_role.hasOne(db.api_marketing_user, { foreignKey: 'apiRoleId', targetKey: 'marketing_role_id', as: 'api_user' });

db.api_marketing_user.hasMany(db.api_marketing_checkin, { foreignKey: 'apiUserId', sourceKey: 'marketing_user_id', as: 'user_checkin' });
db.api_marketing_checkin.belongsTo(db.api_marketing_user, { foreignKey: 'apiUserId', targetKey: 'marketing_user_id', as: 'api_user' });
 
db.api_marketing_checkin.belongsTo(db.api_marketing_checkin_type, { foreignKey: 'checkinTypeId', targetKey: 'checkin_type_id', as: 'checkin_type' });
db.api_marketing_checkin_type.hasOne(db.api_marketing_checkin, { foreignKey: 'checkinTypeId',  targetKey: 'checkin_type_id', as: 'checkin' });

db.api_marketing_work_order.hasMany(db.api_marketing_work_order_location, { foreignKey: 'workOrderId', sourceKey: 'work_order_id', as: 'locations' });
db.api_marketing_work_order_location.belongsTo(db.api_marketing_work_order, { foreignKey: 'workOrderId', targetKey: 'work_order_id', as: 'work_order'  });

db.api_marketing_work_order_location.belongsTo(db.api_type_work, { foreignKey : 'typeWorkId' , targetKey: 'type_work_id', as: 'type_work'});
db.api_type_work.hasOne(db.api_marketing_work_order_location, { foreignKey : 'typeWorkId' , targetKey: 'type_work_id', as: 'location_type_work'});

db.api_marketing_work_order.belongsTo(db.api_marketing_status, { foreignKey: 'statusId', targetKey: 'status_id', as: 'status' });
db.api_marketing_status.hasOne(db.api_marketing_work_order, { foreignKey: 'statusId', targetKey: 'status_id', as: 'work_order'});

db.api_marketing_work_order_location.belongsTo(db.api_marketing_status, { foreignKey : 'statusId' , targetKey: 'status_id', as: 'location_status'});
db.api_marketing_status.hasOne(db.api_marketing_work_order_location, { foreignKey: 'statusId', targetKey: 'status_id', as: 'location'});

db.api_marketing_company.hasMany(db.api_marketing_work_order, { foreignKey: 'companyId', sourceKey: 'company_id', as: 'work_order' });
db.api_marketing_work_order.belongsTo(db.api_marketing_company, { foreignKey: 'companyId', targetKey: 'company_id', as: 'company' });

db.api_marketing_company.hasMany(db.api_marketing_company_messenger_user, { foreignKey: 'companyId', sourceKey: 'company_id', as: 'company' });
db.api_marketing_company_messenger_user.belongsTo(db.api_marketing_company, { foreignKey: 'companyId', targetKey: 'company_id', as: 'user_company' });

db.api_marketing_company_messenger_user.belongsTo(db.api_marketing_user, { foreignKey: 'apiUserId', sourceKey: 'marketing_user_id', as: 'api_user'});
db.api_marketing_user.hasOne(db.api_marketing_company_messenger_user, { foreignKey: 'apiUserId', targetKey: 'marketing_user_id', as: 'user_company'});

db.api_marketing_company.hasMany(db.api_marketing_company_messenger_active, { foreignKey: 'companyId', sourceKey: 'company_id', as: 'user_active'  });
db.api_marketing_company_messenger_active.belongsTo(db.api_marketing_company, { foreignKey: 'companyId', targetKey: 'company_id', as: 'company'});

db.api_marketing_company_messenger_active.belongsTo(db.api_marketing_user, { foreignKey: 'apiUserId', targetKey: 'marketing_user_id', as: 'api_user'});
db.api_marketing_user.hasOne(db.api_marketing_company_messenger_active, { foreignKey: 'apiUserId', targetKey: 'marketing_user_id', as: 'user_active'});

db.api_marketing_work_order_location.hasMany(db.api_marketing_work_order_mile, { foreignKey: 'apiLocationId', sourceKey: 'work_order_location_id', as: 'miles' });
db.api_marketing_work_order_mile.belongsTo(db.api_marketing_work_order_location, { foreignKey: 'apiLocationId', targetKey: 'work_order_location_id', as: 'location' });

db.api_marketing_work_order.belongsTo(db.api_marketing_sender, { foreignKey: 'senderId', targetKey: 'sender_id', as: 'sender'});
db.api_marketing_sender.hasOne(db.api_marketing_work_order, { foreignKey: 'senderId', targetKey: 'sender_id', as: 'work_order'});

db.api_marketing_work_order_location.belongsTo(db.api_marketing_receiver, { foreignKey: 'receiverId', targetKey: 'receiver_id', as: 'receiver'});
db.api_marketing_receiver.hasOne(db.api_marketing_work_order_location, { foreignKey: 'receiverId', targetKey: 'receiver_id', as: 'work_order'});

db.api_marketing_sender.belongsTo(db.district, { foreignKey: 'districtId', targetKey: 'district_id', as: 'district'});
db.district.hasOne(db.api_marketing_sender, { foreignKey: 'districtId', targetKey: 'district_id', as: 'sender_district'});

db.api_marketing_sender.belongsTo(db.amphure, { foreignKey: 'amphureId', targetKey: 'amphure_id', as: 'amphure'});
db.amphure.hasOne(db.api_marketing_sender, { foreignKey: 'amphureId', targetKey: 'amphure_id', as: 'sender_amphure'});

db.api_marketing_sender.belongsTo(db.province, { foreignKey: 'provinceId', targetKey: 'province_id', as: 'province'});
db.province.hasOne(db.api_marketing_sender, { foreignKey: 'provinceId', targetKey: 'province_id', as: 'sender_province'});

db.api_marketing_receiver.belongsTo(db.district, { foreignKey: 'districtId', targetKey: 'district_id', as: 'district'});
db.district.hasOne(db.api_marketing_receiver, { foreignKey: 'districtId', targetKey: 'district_id', as: 'receiver_district'});

db.api_marketing_receiver.belongsTo(db.amphure, { foreignKey: 'amphureId', targetKey: 'amphure_id', as: 'amphure'});
db.amphure.hasOne(db.api_marketing_receiver, { foreignKey: 'amphureId', targetKey: 'amphure_id', as: 'receiver_amphure'});

db.api_marketing_receiver.belongsTo(db.province, { foreignKey: 'provinceId', targetKey: 'province_id', as: 'province'});
db.province.hasOne(db.api_marketing_receiver, { foreignKey: 'provinceId', targetKey: 'province_id', as: 'receiver_province'});

// Project Dipchip ทำ foreignKey
db.pvs_work_order.belongsTo(db.pvs_customer, { foreignKey: 'pvs_customer_id', targetKey: 'pvs_customer_id', as: 'customer'});
db.pvs_customer.hasOne(db.pvs_work_order, { foreignKey: 'pvs_customer_id', targetKey: 'pvs_customer_id', as: 'workorder_customer'});

db.api_marketing_work_order_location.hasMany(db.api_marketing_work_order_signature, { foreignKey: 'apiLocationId', sourceKey: 'work_order_location_id', as: 'signature' });
db.api_marketing_work_order_signature.belongsTo(db.api_marketing_work_order_location, { foreignKey: 'apiLocationId', targetKey: 'work_order_location_id', as: 'location' });


module.exports = db;