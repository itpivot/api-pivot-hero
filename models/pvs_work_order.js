const { Sequelize } = require("sequelize");
const sequelize = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const PvsWorkOrder = sequelize.define(
        'PvsWorkOrder',
        {
            pvs_work_order_id: {
                type: Sequelize.BIGINT,
                field: 'pvs_work_order_id',
                primaryKey: true,
                autoIncrement: true
            },
            sys_date: {
                type: Sequelize.DATE,
                field: 'sys_date'
            },
            barcode: {
               type: Sequelize.STRING,
            },
            pvs_status_id: {
                type: Sequelize.BIGINT,
            },
            pvs_customer_id: {
                type: Sequelize.INTEGER,
                field: 'pvs_customer_id',
                references: {
                    model: 'PvsCustomer',
                    key: 'pvs_customer_id',
                    as: 'pvs_customer_id'
                }
            },
        }, {
            timestamps: false,
            tableName: 'pvs_work_order',
            freezeTableName: true,
        }
    );

    return PvsWorkOrder;
};