const { Sequelize } = require("sequelize");
const sequelize = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const District = sequelize.define(
        'District',
        {
            district_id: {
                type: Sequelize.INTEGER,
                field: 'pvs_districts_id',
                primaryKey: true,
                autoIncrement: true
            },
            code: {
                type: Sequelize.INTEGER,
                field: 'pvs_districts_code',
                defaultValue: null,
            },
            name: {
                type: Sequelize.STRING,
                field: 'pvs_districts_name'
            },
            amphureCode: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'Amphure',
                    key: 'id',
                    as: 'amphureCode'
                },
                field: 'pvs_amphures_code'
            },
            zipcode: {
                type: Sequelize.INTEGER,
            },
            geographyId: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'Geography',
                    key: 'id',
                    as: 'geographyId'
                },
                field: 'pvs_geography_id'
            }
        }, {
            timestamps: false,
            tableName: 'pvs_districts',
            freezeTableName: true,
          
        }
    );

    return District;
};