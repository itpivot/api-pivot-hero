const { Sequelize } = require("sequelize");
const sequelize = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const Branch = sequelize.define(
        'Branch',
        {
            branch_id: {
                type: Sequelize.INTEGER,
                field: 'pvs_branch_id',
                primaryKey: true,
                autoIncrement: true
            },
            branch_name: {
                type: Sequelize.STRING,
            },
            title: {
                type: Sequelize.STRING,
            }
        }, {
            timestamps: false,
            tableName: 'pvs_branch',
            freezeTableName: true,
          
        }
    );

    return Branch;
};