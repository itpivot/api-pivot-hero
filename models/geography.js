const { Sequelize } = require("sequelize");
const sequelize = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const Geography = sequelize.define(
        'Geography',
        {
            geography_id: {
                type: Sequelize.INTEGER,
                field: 'pvs_geography_id',
                primaryKey: true,
                autoIncrement: true
            },
            name: {
                type: Sequelize.STRING,
                field: 'pvs_geography_name'
            },
            title: {
                type: Sequelize.STRING,
            }
        }, {
            timestamps: false,
            tableName: 'pvs_geography',
            freezeTableName: true,
          
        }
    );

    return Geography;
};