const { Sequelize } = require("sequelize");
const sequelize = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const ApiMarketingUser = sequelize.define(
        'ApiMarketingUser',
        {
            marketing_user_id: {
                type: Sequelize.INTEGER,
                field: 'api_marketing_user_id',
                primaryKey: true,
                autoIncrement: true,
            },
            system_timestamp: {
                type: Sequelize.DATE,
            },
            userId: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'User',
                    key: 'user_id',
                    as: 'userId'
                },  
                field: 'pvs_users_id'
            },
            token: {
                type: Sequelize.STRING,
                field: 'token'
            },
            expires: {
                type: Sequelize.STRING,
                field: 'expires'
            },
            apiRoleId: {
                type: Sequelize.INTEGER, 
                references: {
                    model: 'ApiMarketingRole',
                    key: 'id',
                    as: 'apiRoleId'
                },
                field: 'api_marketing_role_id',
            },
            active: {
                type: Sequelize.INTEGER,
                field: 'active'
            }
           
        }, {
            timestamps: false,
            tableName: 'api_marketing_user',
            freezeTableName: true,
          
        }
    );

    return ApiMarketingUser;
};