const { Sequelize } = require("sequelize");
const sequelize = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const ApiDipchipCardReader = sequelize.define(
        'ApiDipchipCardReader',
        {
            api_dipchip_card_reader_id: {
                type: Sequelize.BIGINT,
                field: 'api_dipchip_card_reader_id',
                primaryKey: true,
                autoIncrement: true
            },
            system_timestamp: {
                type: Sequelize.DATE,
                field: 'system_timestamp'
            },
            work_order_barcode: {
                type: Sequelize.STRING,
             },
             work_order_id: {
                type: Sequelize.INTEGER,
                field: 'work_order_id',
                references: {
                    model: 'PvsWorkOrder',
                    key: 'work_order_id',
                    as: 'work_order_id'
                }
            },
            customer_id: {
                type: Sequelize.INTEGER,
                field: 'customer_id',
                references: {
                    model: 'PvsCustomer',
                    key: 'pvs_customer_id',
                    as: 'pvs_customer_id'
                }
            },
            card_id: {
                type: 'VARBINARY(500)',
                field: 'card_id',
            },
            name_title: {
                type: 'VARBINARY(500)',
                field: 'name_title',
            },
            name: {
                type: 'VARBINARY(500)',
                field: 'name',
            },
            name_mid: {
                type: 'VARBINARY(500)',
            },
            surname: {
                type: 'VARBINARY(500)',
            },
            name_title_en: {
                type: 'VARBINARY(500)',
            },
            name_en: {
                type: 'VARBINARY(500)',
            },
            name_mid_en: {
                type: 'VARBINARY(500)',
            },
            surname_en: {
                type: 'VARBINARY(500)',
            },
            address_no: {
                type: Sequelize.STRING,
            },
            address_moo: {
                type: Sequelize.STRING,
            },
            address_sub_alley: {
                type: Sequelize.STRING,
            },
            address_alley: {
                type: Sequelize.STRING,
            },
            road: {
                type: Sequelize.STRING,
            },
            sub_district: {
                type: Sequelize.STRING,
            },
            district: {
                type: Sequelize.STRING,
            },
            province: {
                type: Sequelize.STRING,
            },
            sex: {
                type: Sequelize.STRING,
            },
            date_of_birth: {
                type: Sequelize.STRING,
            },
            location_of_birth: {
                type: Sequelize.STRING,
            },
            date_of_issue: {
                type: Sequelize.STRING,
            },
            date_of_expiry: {
                type: Sequelize.STRING,
            },
            id_of_picture: {
                type: Sequelize.STRING,
            },
            remark: {
                type: Sequelize.STRING,
            },
            picture: {
                type: Sequelize.STRING,
            },
        }, {
            timestamps: false,
            tableName: 'api_dipchip_card_reader',
            freezeTableName: true,
        }
    );

    return ApiDipchipCardReader;
};