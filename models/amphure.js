const { Sequelize } = require("sequelize");
const sequelize = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const Amphure = sequelize.define(
        'Amphure',
        {
            amphure_id: {
                type: Sequelize.INTEGER,
                field: 'pvs_amphures_id',
                primaryKey: true,
                autoIncrement: true,
            },
            code: {
                type: Sequelize.INTEGER,
                field: 'pvs_amphures_code',
                defaultValue: null,
            },
            name: {
                type: Sequelize.STRING,
                field: 'pvs_amphures_name'
            },
            provinceCode: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'Province',
                    key: 'id',
                    as: 'provinceCode'
                },
                field: 'pvs_provinces_code'
            },
        }, {
            timestamps: false,
            tableName: 'pvs_amphures',
            freezeTableName: true,
          
        }
    );

    return Amphure;
};