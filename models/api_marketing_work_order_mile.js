const { Sequelize } = require("sequelize");
const sequelize = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const ApiMarketingWorkOrderMile = sequelize.define(
        'ApiMarketingWorkOrderMile',
        {
            work_order_mile_id: {
                type: Sequelize.BIGINT,
                field: 'api_marketing_work_order_mile_id',
                primaryKey: true,
                autoIncrement: true
            },
            system_timestamp: {
                type: Sequelize.DATE,
            },
            apiLocationId: {
                type: Sequelize.BIGINT,
                references: {
                    model: 'ApiMarketingWorkOrderLocation',
                    key: 'id',
                    as: 'apiLocationId'
                },
                field: 'api_marketing_work_order_location_id',
            },
            mile_number: {
                type: Sequelize.STRING,
            },
            picture: {
                type: Sequelize.STRING,
            },
            latitude: {
                type: Sequelize.STRING,
            },
            longtitude: {
                type: Sequelize.STRING,
            },
            active: {
                type: Sequelize.BOOLEAN,
            },
            location_type: {
                type: Sequelize.STRING,
            },
            apiUserId: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'ApiMarketingUser',
                    key: 'id',
                    as: 'apiUserId'
                },
                field: 'api_marketing_user_id'
            },
        }, {
            timestamps: false,
            tableName: 'api_marketing_work_order_mile',
            freezeTableName: true,
          
        }
    );

    return ApiMarketingWorkOrderMile;
};