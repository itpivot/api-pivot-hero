const { Sequelize } = require("sequelize");
const sequelize = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const ApiMarketingSender = sequelize.define(
        'ApiMarketingSender',
        {
            sender_id: {
                type: Sequelize.BIGINT,
                field: 'api_marketing_sender_id',
                primaryKey: true,
                autoIncrement: true,
            },
            system_timestamp: {
                type: Sequelize.DATE,
            },
            name: {
                type: Sequelize.STRING,
            },
            surname: {
                type: Sequelize.STRING,
            },
            department: {
                type: Sequelize.STRING,
            },
            telephone: {
                type: Sequelize.STRING,
            },
            address: {
                type: Sequelize.STRING,
            },
            districtId: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'District',
                    key: 'id', 
                    as: 'districtId'
                },
                field: 'pvs_districts_id'
            },
            amphureId: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'Amphure',
                    key: 'id', 
                    as: 'amphureId'
                },
                field: 'pvs_amphures_id'
            },
            provinceId: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'Province',
                    key: 'id', 
                    as: 'provinceId'
                },
                field: 'pvs_provinces_id'
            },
        }, {
            timestamps: false,
            tableName: 'api_marketing_sender',
            freezeTableName: true,
          
        }
    );

    return ApiMarketingSender;
};