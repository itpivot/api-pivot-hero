const { Sequelize } = require("sequelize");
const sequelize = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const User = sequelize.define(
        'User',
        {
            user_id: {
                type: Sequelize.INTEGER,
                field: 'pvs_users_id',
                primaryKey: true,
                autoIncrement: true
            },
            system_date: {
                type: Sequelize.DATE,
                field: 'system_date',
                defaultValue: Sequelize.NOW
            },
            username: {
                type: Sequelize.STRING,
                field: 'username'
            },
            password: {
                type: Sequelize.STRING,
                field: 'password'
            },
            create_date: {
                type: Sequelize.DATE,
                field: 'create_date'
            },
            active: {
                type: Sequelize.INTEGER,
                field: 'active'
            },
            active: {
                type: Sequelize.INTEGER,
                field: 'active'
            },
            employeeId: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'Employee',
                    key: 'id',
                    as: 'employeeId'
                },
                field: 'pvs_employee_id',
            },
            roleId: {
                type: Sequelize.INTEGER,
                field: 'pvs_users_role_id',
                references: {
                    model: 'Role',
                    key: 'id',
                    as: 'roleId'
                },
                referencesKey: 'pvs_users_role_id'
            },
            pvs_sale_team_id: {
                type: Sequelize.INTEGER,
                field: 'pvs_sale_team_id'
            },
            pvs_bank_id: {
                type: Sequelize.INTEGER,
                field: 'pvs_bank_id'
            },
            area_code: {
                type: Sequelize.STRING,
                field: 'area_code'
            },
            first_login: {
                type: Sequelize.INTEGER,
                field: 'first_login'
            },
            isLogin: {
                type: Sequelize.INTEGER,
                field: 'isLogin'
            },
            isFailed: {
                type: Sequelize.INTEGER,
                field: 'isFailed'
            },
            last_change_password: {
                type: Sequelize.DATE,
                field: 'last_change_password'
            },
            hash: {
                type: Sequelize.STRING,
                field: 'hash'
            },
            verify: {
                type: Sequelize.INTEGER,
                field: 'verify'
            }
        }, {
            timestamps: false,
            tableName: 'pvs_users',
            freezeTableName: true,
          
        }
    );

    return User;
};