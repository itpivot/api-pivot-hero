const { Sequelize } = require("sequelize");
const sequelize = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const Employee = sequelize.define(
        'Employee',
        {
            employee_id: {
                type: Sequelize.INTEGER,
                field: 'pvs_employee_id',
                primaryKey: true,
                autoIncrement: true
            },
            system_date: {
                type: Sequelize.DATE,
                field: 'system_date',
                defaultValue: Sequelize.NOW
            },
            code: {
                type: Sequelize.STRING,
                field: 'code'
            },
            temp_code: {
                type: Sequelize.STRING,
                field: 'temp_code',
                defaultValue: null
            },
            id_card: {
                type: 'VARBINARY(500)',
                field: 'id_card',
                defaultValue: null
            },
            firstname: {
                type: 'VARBINARY(500)',
                field: 'firstname'
            },
            lastname: {
                type: 'VARBINARY(500)',
                field: 'lastname'
            },
            start_date: {
                type: Sequelize.DATEONLY,
                field: 'start_date',
                defaultValue: null
            },
            resign_date: {
                type: Sequelize.DATEONLY,
                field: 'resign_date',
                defaultValue: null
            },
            telephone: {
                type: 'VARBINARY(500)',
                field: 'telephone',
                defaultValue: null
            },
            branchId: {
                type: Sequelize.INTEGER,
                field: 'pvs_branch_id',
                references: {
                    model: 'Bracnch',
                    key: 'id',
                    as: 'branchId'
                },
                defaultValue: null
            },
            departmentId: {
                type: Sequelize.INTEGER,
                field: 'pvs_department_id',
                references: {
                    model: 'Department',
                    key: 'id',
                    as: 'departmentId'
                },
                defaultValue: null
            },
            is_encrypt: {
                type: Sequelize.INTEGER,
                field: 'is_encrypt',
                defaultValue: 0
            }
        },
        {
            timestamps: false,
            tableName: 'pvs_employee',
            freezeTableName: true,
          
        }
    );

    return Employee;
}