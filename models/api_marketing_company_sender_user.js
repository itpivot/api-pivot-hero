const { Sequelize } = require("sequelize");
const sequelize = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const ApiMarketingCompanySenderUser = sequelize.define(
        'ApiMarketingCompanySenderUser',
        {
            company_sender_user_id: {
                type: Sequelize.INTEGER,
                field: 'api_marketing_company_sender_user_id',
                primaryKey: true,
                autoIncrement: true,
            },
            system_timestamp: {
                type: Sequelize.DATE,
            },
            companyId: {
                type: Sequelize.BIGINT, 
                references: {
                    model: 'ApiMarketingCompany',
                    key: 'id',
                    as: 'companyId'
                },
                field: 'api_marketing_company_id'
            },
            companyUserId: {
                type: Sequelize.BIGINT,
                references: {
                    model: 'ApiMarketingCompany',
                    key: 'id',
                    as: 'companyUserId'
                },
                field: 'api_marketing_company_user_id'
            },
        }, {
            timestamps: false,
            tableName: 'api_marketing_company_sender_user',
            freezeTableName: true,
          
        }
    );

    return ApiMarketingCompanySenderUser;
};