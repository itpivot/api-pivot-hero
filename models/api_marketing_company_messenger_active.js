const { Sequelize } = require("sequelize");
const sequelize = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const ApiMarketingCompanyMessengerActive = sequelize.define(
        'ApiMarketingCompanyMessengerActive',
        {
            company_messenger_active_id: {
                type: Sequelize.BIGINT,
                field: 'api_marketing_company_messenger_active_id',
                primaryKey: true,
                autoIncrement: true
            },
            system_timestamp: {
                type: Sequelize.DATE,
            },
            started_at: {
                type: Sequelize.DATE,
                allowNull: true
            },
            companyId: {
                type: Sequelize.BIGINT, 
                references: {
                    model: 'ApiMarketingCompany',
                    key: 'id',
                    as: 'companyId'
                },
                field: 'api_marketing_company_id'
            },
            apiUserId: {
                type: Sequelize.BIGINT,
                references: {
                    model: 'ApiMarketingUser',
                    key: 'id',
                    as: 'apiUserId'
                },
                field: 'api_marketing_user_id'
            },
            active: {
                type: Sequelize.BOOLEAN,
            }
        }, {
            timestamps: false,
            tableName: 'api_marketing_company_messenger_active',
            freezeTableName: true,
          
        }
    );

    return ApiMarketingCompanyMessengerActive;
};