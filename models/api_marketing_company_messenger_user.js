const { Sequelize } = require("sequelize");
const sequelize = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const ApiMarketingCompanyMessengerUser = sequelize.define(
        'ApiMarketingCompanyMessengerUser',
        {
            company_messenger_user_id: {
                type: Sequelize.BIGINT,
                field: 'api_marketing_company_messenger_user_id',
                primaryKey: true,
                autoIncrement: true,
            },
            system_timestamp: {
                type: Sequelize.DATE,
            },
            companyId: {
                type: Sequelize.BIGINT, 
                references: {
                    model: 'ApiMarketingCompany',
                    key: 'id',
                    as: 'companyId'
                },
                field: 'api_marketing_company_id'
            },
            apiUserId: {
                type: Sequelize.BIGINT,
                references: {
                    model: 'ApiMarketingUser',
                    key: 'id',
                    as: 'apiUserId'
                },
                field: 'api_marketing_user_id'
            },
        }, {
            timestamps: false,
            tableName: 'api_marketing_company_messenger_user',
            freezeTableName: true,
          
        }
    );

    return ApiMarketingCompanyMessengerUser;
};