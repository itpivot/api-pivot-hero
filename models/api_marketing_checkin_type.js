const { Sequelize } = require("sequelize");
const sequelize = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const ApiMarketingCheckinType = sequelize.define(
        'ApiMarketingCheckinType',
        {
            checkin_type_id: {
                type: Sequelize.INTEGER,
                field: 'api_marketing_checkin_type_id',
                primaryKey: true,
                autoIncrement: true
            },
            name: {
                type: Sequelize.STRING,
                field: 'api_maketing_checkin_type_name',
            }
           
        }, {
            timestamps: false,
            tableName: 'api_marketing_checkin_type',
            freezeTableName: true,
        }
    );

    return ApiMarketingCheckinType;
};