const { Sequelize } = require("sequelize");
const sequelize = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const ApiTypeWork = sequelize.define(
        'ApiTypeWork',
        {
            type_work_id: {
                type: Sequelize.INTEGER,
                field: 'api_type_work_id',
                primaryKey: true,
                autoIncrement: true
            },
            system_timestamp: {
                type: Sequelize.DATE,
            },
            type_name: {
                type: Sequelize.STRING,
            }
        }, {
            timestamps: false,
            tableName: 'api_type_work',
            freezeTableName: true,
          
        }
    );

    return ApiTypeWork;
};