const { Sequelize } = require("sequelize");
const sequelize = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const PvsCustomer = sequelize.define(
        'PvsCustomer',
        {
            pvs_customer_id: {
                type: Sequelize.BIGINT,
                field: 'pvs_customer_id',
                primaryKey: true,
                autoIncrement: true
            },
            system_date: {
                type: Sequelize.DATE,
                field: 'system_date'
            },
            cus_name: {
               type: 'VARBINARY(500)',
               field: 'cus_name'
            },
            cus_lastname: {
                type: 'VARBINARY(500)',
                field: 'cus_lastname'
            },
        }, {
            timestamps: false,
            tableName: 'pvs_customer',
            freezeTableName: true,
        }
    );

    return PvsCustomer;
};