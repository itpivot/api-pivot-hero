const { Sequelize } = require("sequelize");
const sequelize = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const ApiMarketingRole = sequelize.define(
        'ApiMarketingRole',
        {
            marketing_role_id: {
                type: Sequelize.INTEGER,
                field: 'api_marketing_role_id',
                primaryKey: true,
                autoIncrement: true,
            },
            role_name: {
                type: Sequelize.STRING,
                field: 'api_marketing_role_name',
            }
        }, {
            timestamps: false,
            tableName: 'api_marketing_role',
            freezeTableName: true,
          
        }
    );

    return ApiMarketingRole;
};