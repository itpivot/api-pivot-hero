const { Sequelize } = require("sequelize");
const sequelize = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const ApiMarketingCompany = sequelize.define(
        'ApiMarketingCompany',
        {
            company_id: {
                type: Sequelize.BIGINT,
                field: 'api_marketing_company_id',
                primaryKey: true,
                autoIncrement: true
            },
            system_timestamp: {
                type: Sequelize.DATE,
            },
            company_code: {
                type: Sequelize.STRING,
            },
            company_name: {
                type: Sequelize.STRING,
            },
            contact_name: {
                type: Sequelize.STRING,
            },
            telephone_1: {
                type: Sequelize.STRING,
            },
            telephone_2: {
                type: Sequelize.STRING,
                allowNull: true
            },
            email: {
                type: Sequelize.STRING,
                allowNull: true
            },
            address: {
                type: Sequelize.STRING,
            },
            districtId: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'pvsDistrict',
                    key: 'id', 
                    as: 'districtId'
                },
                field: 'pvs_districts_id'
            },
            amphureId: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'pvsAmphure',
                    key: 'id', 
                    as: 'amphureId'
                },
                field: 'pvs_amphures_id'
            },
            provinceId: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'pvsProvince',
                    key: 'id', 
                    as: 'provinceId'
                },
                field: 'pvs_provinces_id'
            },
            tax_number: {
                type: Sequelize.STRING,
            },
            latitude: {
                type: Sequelize.STRING,
            },
            longtitude: {
                type: Sequelize.STRING,
            },

        }, {
            timestamps: false,
            tableName: 'api_marketing_company',
            freezeTableName: true,
        }
    );

    return ApiMarketingCompany;
};