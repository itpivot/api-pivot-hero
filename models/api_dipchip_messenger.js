const { Sequelize } = require("sequelize");
const sequelize = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const ApiDipchipMessenger = sequelize.define(
        'ApiDipchipMessenger',
        {
            api_dipchip_messenger_id: {
                type: Sequelize.BIGINT,
                field: 'api_dipchip_messenger_id',
                primaryKey: true,
                autoIncrement: true
            },
            system_timestamp: {
                type: Sequelize.DATE,
                field: 'system_timestamp'
            },
            card_id: {
               type: Sequelize.STRING,
            },
            name_title: {
                type: Sequelize.STRING,
            },
            name: {
                type: Sequelize.STRING,
            },
            name_mid: {
                type: Sequelize.STRING,
            },
            surname: {
                type: Sequelize.STRING,
            },
            name_title_en: {
                type: Sequelize.STRING,
            },
            name_en: {
                type: Sequelize.STRING,
            },
            name_mid_en: {
                type: Sequelize.STRING,
            },
            surname_en: {
                type: Sequelize.STRING,
            },
            address_no: {
                type: Sequelize.STRING,
            },
            address_moo: {
                type: Sequelize.STRING,
            },
            address_sub_alley: {
                type: Sequelize.STRING,
            },
            address_alley: {
                type: Sequelize.STRING,
            },
            road: {
                type: Sequelize.STRING,
            },
            sub_district: {
                type: Sequelize.STRING,
            },
            district: {
                type: Sequelize.STRING,
            },
            province: {
                type: Sequelize.STRING,
            },
            sex: {
                type: Sequelize.STRING,
            },
            date_of_birth: {
                type: Sequelize.STRING,
            },
            location_of_birth: {
                type: Sequelize.STRING,
            },
            date_of_issue: {
                type: Sequelize.STRING,
            },
            date_of_expiry: {
                type: Sequelize.STRING,
            },
            id_of_picture: {
                type: Sequelize.STRING,
            },
            remark: {
                type: Sequelize.STRING,
            },
            picture: {
                type: Sequelize.STRING,
            },
        }, {
            timestamps: false,
            tableName: 'api_dipchip_messenger',
            freezeTableName: true,
        }
    );

    return ApiDipchipMessenger;
};