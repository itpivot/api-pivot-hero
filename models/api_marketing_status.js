const { Sequelize } = require("sequelize");
const sequelize = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const ApiMarketingStatus = sequelize.define(
        'ApiMarketingStatus',
        {
            status_id: {
                type: Sequelize.INTEGER,
                field: 'api_marketing_status_id',
                primaryKey: true,
                autoIncrement: true,
            },
            system_timestamp: {
                type: Sequelize.DATE,
            },
            status_name: {
                type: Sequelize.STRING,
            }
        }, {
            timestamps: false,
            tableName: 'api_marketing_status',
            freezeTableName: true,
          
        }
    );

    return ApiMarketingStatus;
};