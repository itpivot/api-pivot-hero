const { Sequelize } = require("sequelize");
const sequelize = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const ApiMarketingWorkOrder = sequelize.define(
        'ApiMarketingWorkOrder',
        {
            work_order_id: {
                type: Sequelize.BIGINT,
                field: 'api_marketing_work_order_id',
                primaryKey: true,
                autoIncrement: true
            },
            system_timestamp: {
                type: Sequelize.DATE,
            },
            barcode: {
                type: Sequelize.STRING,
            },
            date_order: {
                type: Sequelize.DATEONLY,
            },
            date_meet: {
                type: Sequelize.DATEONLY,
            },
            date_closejob: {
                type: Sequelize.DATE,
                allowNull: true
            },
            date_receive: {
                type: Sequelize.DATE,
                allowNull: true
            },
            companyId: {
                type: Sequelize.BIGINT, 
                references: {
                    model: 'ApiMarketingCompany',
                    key: 'id',
                    as: 'companyId'
                },
                field: 'api_marketing_company_id'
            },
            senderId: {
                type: Sequelize.BIGINT, 
                references: {
                    model: 'ApiMarketingSender',
                    key: 'id',
                    as: 'senderId'
                },
                field: 'api_marketing_sender_id'
            },
            statusId: {
                type: Sequelize.INTEGER, 
                references: {
                    model: 'ApiMarketingStatus',
                    key: 'id',
                    as: 'statusId'
                },
                field: 'api_marketing_status_id'
            },
            companyUserId: {
                type: Sequelize.BIGINT,
                references: {
                    model: 'ApiMarketingCompany',
                    key: 'id',
                    as: 'companyUserId'
                },
                field: 'api_marketing_company_user_id'
            },
            apiUserId: {
                type: Sequelize.BIGINT,
                references: {
                    model: 'ApiMarketingUser',
                    key: 'id',
                    as: 'apiUserId'
                },
                field: 'api_marketing_user_id'
            },
            active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1
            }
        }, {
            timestamps: false,
            tableName: 'api_marketing_work_order',
            freezeTableName: true,
        }
    );
    return ApiMarketingWorkOrder;
};