const { Sequelize } = require("sequelize");
const sequelize = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const ApiMarketingCheckin = sequelize.define(
        'ApiMarketingCheckin',
        {
            checkin_id: {
                type: Sequelize.BIGINT,
                field: 'api_marketing_checkin_id',
                primaryKey: true,
                autoIncrement: true
            },
            system_timestamp: {
                type: Sequelize.DATE,
                field: 'system_timestamp'
            },
            apiUserId: {
                type: Sequelize.BIGINT,
                references: {
                    model: 'ApiMarketingUser',
                    key: 'id',
                    as: 'apiUserId'
                },
                field: 'api_marketing_user_id'
            },
            checkinTypeId: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'ApiMarketingCheckinType',
                    key: 'id',
                    as: 'checkinTypeId'
                },
                field: 'api_marketing_checkin_type_id'
            },
           latitude :{
                type: Sequelize.STRING,
           },
           longtitude: {
               type: Sequelize.STRING,
           }
           
        }, {
            timestamps: false,
            tableName: 'api_marketing_checkin',
            freezeTableName: true,
        }
    );

    return ApiMarketingCheckin;
};