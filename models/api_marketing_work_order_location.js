const { Sequelize } = require("sequelize");
const sequelize = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const ApiMarketingWorkOrderLocation = sequelize.define(
        'ApiMarketingWorkOrderLocation',
        {
            work_order_location_id: {
                type: Sequelize.BIGINT,
                field: 'api_marketing_work_order_location_id',
                primaryKey: true,
                autoIncrement: true
            },
            system_timestamp: {
                type: Sequelize.DATE,
            },
            date_closejob: {
                type: Sequelize.DATE,
            },
            barcode: {
                type: Sequelize.STRING,
            },
            workOrderId: {
                type: Sequelize.BIGINT, 
                references: {
                    model: 'ApiMarketingWorkOrder',
                    key: 'id',
                    as: 'workOrderId'
                },
                field: 'api_marketing_work_order_id'
            },
            receiverId: {
                type: Sequelize.BIGINT, 
                references: {
                    model: 'ApiMarketingReceiver',
                    key: 'id',
                    as: 'receiverId'
                },
                field: 'api_marketing_receiver_id'
            },
            statusId: {
                type: Sequelize.INTEGER, 
                references: {
                    model: 'ApiMarketingStatus',
                    key: 'id',
                    as: 'statusId'
                },
                field: 'api_marketing_status_id'
            },
            price: {
                type: Sequelize.DECIMAL(19, 2),
                defaultValue: 0.00,
                field: 'price'
            },
            typeWorkId: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'ApiTypeWork',
                    key: 'id',
                    as: 'typeWorkId'
                },
                field: 'api_type_work_id'
            },
            detail_type_work: {
                type: Sequelize.STRING(200),
            },
            order_remark: {
                type: Sequelize.TEXT,
            },
            messenger_remark: {
                type: Sequelize.TEXT,
            },
            active: {
                type: Sequelize.BOOLEAN,
            },
            apiUserId: {
                type: Sequelize.BIGINT,
                references: {
                    model: 'ApiMarketingUser',
                    key: 'id',
                    as: 'apiUserId'
                },
                field: 'api_marketing_user_id'
            },
        }, {
            timestamps: false,
            tableName: 'api_marketing_work_order_location',
            freezeTableName: true,
        }
    );

    return ApiMarketingWorkOrderLocation;
};