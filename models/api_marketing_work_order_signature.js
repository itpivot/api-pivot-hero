const { Sequelize } = require("sequelize");
const sequelize = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const ApiMarketingWorkOrderSignature = sequelize.define(
        'ApiMarketingWorkOrderSignature',
        {
            work_order_signature_id: {
                type: Sequelize.BIGINT,
                field: 'api_marketing_work_order_signature_id',
                primaryKey: true,
                autoIncrement: true
            },
            system_timestamp: {
                type: Sequelize.DATE,
            },
            apiLocationId: {
                type: Sequelize.BIGINT,
                references: {
                    model: 'ApiMarketingWorkOrderLocation',
                    key: 'id',
                    as: 'apiLocationId'
                },
                field: 'api_marketing_work_order_location_id',
            },
           
            picture: {
                type: Sequelize.STRING,
            },
            apiUserId: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'ApiMarketingUser',
                    key: 'id',
                    as: 'apiUserId'
                },
                field: 'api_marketing_user_id'
            },
        }, {
            timestamps: false,
            tableName: 'api_marketing_work_order_signature',
            freezeTableName: true,
          
        }
    );

    return ApiMarketingWorkOrderSignature;
};