const { Sequelize } = require("sequelize");
const sequelize = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const ApiDipchipCardReaderLog = sequelize.define(
        'ApiDipchipCardReaderLog',
        {
            api_dipchip_card_reader_log_id: {
                type: Sequelize.BIGINT,
                field: 'api_dipchip_card_reader_log_id',
                primaryKey: true,
                autoIncrement: true
            },
            system_timestamp: {
                type: Sequelize.DATE,
                field: 'system_timestamp'
            },
            work_order_barcode: {
                type: Sequelize.STRING,
             },
             work_order_id: {
                type: Sequelize.INTEGER,
                field: 'work_order_id',
                references: {
                    model: 'PvsWorkOrder',
                    key: 'work_order_id',
                    as: 'work_order_id'
                }
            },
            customer_id: {
                type: Sequelize.INTEGER,
                field: 'customer_id',
                references: {
                    model: 'PvsCustomer',
                    key: 'pvs_customer_id',
                    as: 'pvs_customer_id'
                }
            },
            status_id: {
                type: Sequelize.INTEGER,
            },
            log_description: {
                type: Sequelize.STRING,
            }
        }, {
            timestamps: false,
            tableName: 'api_dipchip_card_reader_log',
            freezeTableName: true,
        }
    );

    return ApiDipchipCardReaderLog;
};