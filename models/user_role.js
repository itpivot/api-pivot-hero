const { Sequelize } = require("sequelize");
const sequelize = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const Role = sequelize.define(
        'Role',
        {
            users_role_id: {
                type: Sequelize.INTEGER,
                field: 'pvs_users_role_id',
                primaryKey: true,
                autoIncrement: true
            },
            name: {
                type: Sequelize.STRING,
            },
            role_path: {
                type: Sequelize.STRING,
            },
            img_path: {
                type: Sequelize.STRING,
            }
        }, {
            timestamps: false,
            tableName: 'pvs_users_role',
            freezeTableName: true,
          
        }
    );

    return Role;
};