const { Sequelize } = require("sequelize");
const sequelize = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const Province = sequelize.define(
        'Province',
        {
            province_id: {
                type: Sequelize.INTEGER,
                field: 'pvs_provinces_id',
                primaryKey: true,
                autoIncrement: true
            },
            code: {
                type: Sequelize.INTEGER,
                field: 'pvs_provinces_code',
                defaultValue: null,
            },
            name: {
                type: Sequelize.STRING,
                field: 'pvs_provinces_name'
            },
            geographyId: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'Geography',
                    key: 'id',
                    as: 'geographyId'
                },
                field: 'pvs_geography_id'
            }
        }, {
            timestamps: false,
            tableName: 'pvs_provinces',
            freezeTableName: true,
          
        }
    );

    return Province;
};