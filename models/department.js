const { Sequelize } = require("sequelize");
const sequelize = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const Department = sequelize.define(
        'Department',
        {
            department_id: {
                type: Sequelize.INTEGER,
                field: 'pvs_department_id',
                primaryKey: true,
                autoIncrement: true
            },
            name: {
                type: Sequelize.STRING,
            }
        }, {
            timestamps: false,
            tableName: 'pvs_deparment',
            freezeTableName: true,
          
        }
    );

    return Department;
};