const { Sequelize } = require("sequelize");
const sequelize = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const ApiMarketingCompanyUser = sequelize.define(
        'ApiMarketingCompanyUser',
        {
            company_user_id: {
                type: Sequelize.INTEGER,
                field: 'api_marketing_company_user_id',
                primaryKey: true,
                autoIncrement: true,
            },
            system_timestamp: {
                type: Sequelize.DATE,
            },
            userId: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'User',
                    key: 'id',
                    as: 'userId'
                },
                field: 'pvs_users_id'
            },
            roleId: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'Role',
                    key: 'id',
                    as: 'roleId'
                },
                field: 'pvs_users_role_id'
            },
            active: {
                type: Sequelize.BOOLEAN,
            }
        }, {
            timestamps: false,
            tableName: 'api_marketing_company_user',
            freezeTableName: true,
          
        }
    );

    return ApiMarketingCompanyUser;
};